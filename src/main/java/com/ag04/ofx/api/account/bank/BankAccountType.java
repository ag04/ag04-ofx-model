package com.ag04.ofx.api.account.bank;

/**
 * Enum representing standard OFX bank account types.
 *
 * Created by dmadunic on 20/08/2016.
 */
public enum BankAccountType {
    CHECKING, SAVINGS, MONEYMRKT, CREDITLINE, CD;
}
