package com.ag04.ofx.api.account.bank;

import com.ag04.ofx.api.account.AccountStatus;
import com.ag04.ofx.api.account.CommonAccountInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.statement.BalanceAggregateEssence;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("BANKACCTINFO")
@XmlRootElement(name="BANKACCTINFO")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankAccountInfo extends CommonAccountInfo {

	@XStreamAlias("BANKACCTFROM")
	@XmlElement(name="BANKACCTFROM")
	private BankAccountFromAggregate accountAggregate;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("accountAggregate", accountAggregate);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	public BankAccountFromAggregate getAccountAggregate() {
		return accountAggregate;
	}

	public void setAccountAggregate(BankAccountFromAggregate accountAggregate) {
		this.accountAggregate = accountAggregate;
	}

}
