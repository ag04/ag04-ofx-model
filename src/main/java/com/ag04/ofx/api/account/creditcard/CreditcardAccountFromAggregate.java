package com.ag04.ofx.api.account.creditcard;

import com.ag04.ofx.api.account.bank.BankAccountAggregate;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("CCACCTFROM")
@XmlRootElement(name="CCACCTFROM")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardAccountFromAggregate extends CreditCardAccountAggregate {

    public CreditCardAccountFromAggregate() {
        // default constructor ...
    }

    public CreditCardAccountFromAggregate(CreditCardAccountAggregate ccAccountAggregate) {
        super(ccAccountAggregate);
    }
}
