package com.ag04.ofx.api.account.bank;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author domagoj
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BankAccountAggregate {
	
	/**
	 * Bank identifier, A-9 /M
	 * Use of this field varies from country to Country:
	 * In GE and IT it is number given by national agencies.
	 *
	 */
	@XStreamAlias("BANKID")
	@XmlElement(name="BANKID")
	private String bankId;

	/**
	 * Branch identifier. May be required for some non-US banks, A-22
	 */
	@XStreamAlias("BRANCHID")
	@XmlElement(name="BRANCHID")
	private String branchId;
	
	/**
	 * Account number, A-22 /M
	 */
	@XStreamAlias("ACCTID")
	@XmlElement(name="ACCTID")
	private String accountNumber;
	
	/**
	 * Checksum, A-22
	 */
	@XStreamAlias("ACCTKEY")
	@XmlElement(name="ACCTKEY")
	private String accessKey;
	
	/**
	 * Type of account, see section 11.3.1.1 /M
	 */
	@XStreamAlias("ACCTTYPE")
	@XmlElement(name="ACCTTYPE")
	private BankAccountType type;
	
	public BankAccountAggregate() {
		//
	}

	public BankAccountAggregate(final String bankId, final String accountNumber, final BankAccountType type) {
		this.bankId = bankId;
		this.accountNumber = accountNumber;
		this.type = type;
	}
	
	public BankAccountAggregate(BankAccountAggregate bankAccountAggregate) {
		this.bankId = bankAccountAggregate.getBankId();
		this.branchId = bankAccountAggregate.getBranchId();
		this.accountNumber = bankAccountAggregate.getAccountNumber();
		this.accessKey = bankAccountAggregate.getAccessKey();
		this.type = bankAccountAggregate.getType();
	}
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("bankId", bankId);
		sbuilder.append("branchId", branchId);
		sbuilder.append("accountNumber", accountNumber);
		sbuilder.append("accessKey", accessKey);
		sbuilder.append("type", type);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public BankAccountType getType() {
		return type;
	}

	public void setType(BankAccountType type) {
		this.type = type;
	}
}
