package com.ag04.ofx.api.account;

import com.ag04.ofx.api.account.creditcard.CreditCardAccountInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.account.bank.BankAccountInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("ACCTINFO")
@XmlRootElement(name="ACCTINFO")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountInfo {

	/**
	 * Name/User defined nickname for the account, A-32.
	 * If present indicates the server may be obfuscating account numbers per Section 8.5.1.
	 *
	 */
	@XStreamAlias("NAME")
	@XmlElement(name="NAME")
	private String name;

	@XStreamAlias("DESC")
	@XmlElement(name="DESC")
	private String description;
	
	@XStreamAlias("PHONE")
	@XmlElement(name="PHONE")
	private String phone;
	
	@XStreamAlias("BANKACCTINFO")
	@XmlElement(name="BANKACCTINFO")
	private BankAccountInfo bankAccountInfo;

	@XStreamAlias("CCACCTINFO")
	@XmlElement(name="CCACCTINFO")
	private CreditCardAccountInfo creditCardAccountInfo;
	
	//TODO: add other account types here ...
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("name", name);
		sbuilder.append("description", description);
		sbuilder.append("phone", phone);
		sbuilder.append("bankAccountInfo", bankAccountInfo);
		sbuilder.append("creditCardAccountInfo", creditCardAccountInfo);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BankAccountInfo getBankAccountInfo() {
		return bankAccountInfo;
	}

	public void setBankAccountInfo(BankAccountInfo bankAccountInfo) {
		this.bankAccountInfo = bankAccountInfo;
	}

	public CreditCardAccountInfo getCreditCardAccountInfo() {
		return creditCardAccountInfo;
	}

	public void setCreditCardAccountInfo(CreditCardAccountInfo creditCardAccountInfo) {
		this.creditCardAccountInfo = creditCardAccountInfo;
	}
}
