package com.ag04.ofx.api.account;

/**
 * Created by dmadunic on 21/08/2016.
 */
public enum AccountClassification {
    PERSONAL, BUSINESS, CORPORATE, OTHER;
}
