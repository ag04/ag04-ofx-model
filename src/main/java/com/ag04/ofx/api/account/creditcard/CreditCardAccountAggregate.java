package com.ag04.ofx.api.account.creditcard;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardAccountAggregate {
	
	/**
	 * Account number, A-22
	 */
	@XStreamAlias("ACCTID")
	@XmlElement(name="ACCTID")
	private String accountNumber;
	
	/**
	 * Checksum for international banks, A-22
	 */
	@XStreamAlias("ACCTKEY")
	@XmlElement(name="ACCTKEY")
	private String accessKey;

	public CreditCardAccountAggregate() {
		// default constructor
	}

	public CreditCardAccountAggregate(final String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public CreditCardAccountAggregate(CreditCardAccountAggregate ccAccountAggregate) {
		this.accountNumber = ccAccountAggregate.getAccountNumber();
		this.accessKey = ccAccountAggregate.getAccessKey();
	}
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("accountNumber", accountNumber);
		sbuilder.append("accessKey", accessKey);
		return sbuilder.toString();
	}
	
	//--- set  / get methods --------------------------------------------------

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
}
