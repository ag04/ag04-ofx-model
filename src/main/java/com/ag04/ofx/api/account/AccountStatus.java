package com.ag04.ofx.api.account;

/**
 * Enum representing standard OFX account statuses:
 * <ul>
 *     <li>AVAIL = Available, but not yet requested</li>
 *     <li>PEND = Requested, but not yet available</li>
 *     <li>ACTIVE = In use</li>
 * </ul>
 * And two additional statuses used by ofxgw services: CLOSED and BLOCKED.
 *
 * These statuses are used internally by ofxgwservices and Accounts in these statuses are NEVER sent to the client.
 *
 * Created by dmadunic on 20/08/2016.
 */
public enum AccountStatus {
    AVAIL, PEND, ACTIVE, CLOSED, BLOCKED;
}
