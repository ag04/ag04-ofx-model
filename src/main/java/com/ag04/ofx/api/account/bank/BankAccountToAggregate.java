package com.ag04.ofx.api.account.bank;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("BANKACCTTO")
@XmlRootElement(name="BANKACCTTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankAccountToAggregate extends BankAccountAggregate {

	public BankAccountToAggregate() {}
	
	public BankAccountToAggregate(BankAccountAggregate bankAccountAggregate) {
		super(bankAccountAggregate);
	}
}
