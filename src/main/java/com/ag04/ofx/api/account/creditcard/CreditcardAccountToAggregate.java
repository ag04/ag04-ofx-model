package com.ag04.ofx.api.account.creditcard;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("CCACCTTO")
@XmlRootElement(name="CCACCTTO")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardAccountToAggregate extends CreditCardAccountAggregate {

}
