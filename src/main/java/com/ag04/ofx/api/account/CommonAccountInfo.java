package com.ag04.ofx.api.account;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CommonAccountInfo {

    @XStreamAlias("SVCSTATUS")
    @XmlElement(name="SVCSTATUS")
    private AccountStatus status;

    /**
     *  Account classification; see section 11.3.3.1
     */
    @XStreamAlias("ACCTCLASSIFICATION")
    @XmlElement(name="ACCTCLASSIFICATION")
    private AccountClassification classification;

    @XStreamAlias("SUPTXDL")
    @XmlElement(name="SUPTXDL")
    private Boolean trxDownloadSupported;

    @XStreamAlias("XFERSRC")
    @XmlElement(name="XFERSRC")
    private Boolean enabledAsInterbankSource;

    @XStreamAlias("XFERDEST")
    @XmlElement(name="XFERDEST")
    private Boolean enabledAsInterbankDestination;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("status", status);
        sbuilder.append("trxDownloadSupported", trxDownloadSupported);
        sbuilder.append("enabledAsInterbankSource", enabledAsInterbankSource);
        sbuilder.append("enabledAsInterbankDestination", enabledAsInterbankDestination);
        sbuilder.append("classification", classification);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public Boolean getTrxDownloadSupported() {
        return trxDownloadSupported;
    }

    public void setTrxDownloadSupported(Boolean trxDownloadSupported) {
        this.trxDownloadSupported = trxDownloadSupported;
    }

    public Boolean getEnabledAsInterbankSource() {
        return enabledAsInterbankSource;
    }

    public void setEnabledAsInterbankSource(Boolean enabledAsInterbankSource) {
        this.enabledAsInterbankSource = enabledAsInterbankSource;
    }

    public Boolean getEnabledAsInterbankDestination() {
        return enabledAsInterbankDestination;
    }

    public void setEnabledAsInterbankDestination(Boolean enabledAsInterbankDestination) {
        this.enabledAsInterbankDestination = enabledAsInterbankDestination;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public AccountClassification getClassification() {
        return classification;
    }

    public void setClassification(AccountClassification classification) {
        this.classification = classification;
    }
}
