package com.ag04.ofx.api.account.creditcard;

import com.ag04.ofx.api.account.CommonAccountInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("CCACCTINFO")
@XmlRootElement(name="CCACCTINFO")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardAccountInfo extends CommonAccountInfo {

	@XStreamAlias("CCACCTFROM")
	@XmlElement(name="CCACCTFROM")
	private CreditCardAccountFromAggregate accountAggregate;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("accountAggregate", accountAggregate);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	
	public CreditCardAccountFromAggregate getAccountAggregate() {
		return accountAggregate;
	}

	public void setAccountAggregate(CreditCardAccountFromAggregate accountAggregate) {
		this.accountAggregate = accountAggregate;
	}

}
