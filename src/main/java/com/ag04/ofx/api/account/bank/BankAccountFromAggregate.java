package com.ag04.ofx.api.account.bank;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("BANKACCTFROM")
@XmlRootElement(name="BANKACCTFROM")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankAccountFromAggregate extends BankAccountAggregate {
	
	public BankAccountFromAggregate() {
		//
	}
	
	public BankAccountFromAggregate(BankAccountAggregate bankAccountAggregate) {
		super(bankAccountAggregate);
	}
}
