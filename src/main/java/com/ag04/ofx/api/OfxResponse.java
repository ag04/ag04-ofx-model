package com.ag04.ofx.api;

import com.ag04.ofx.api.creditcard.CreditCardMessageSetResponse;
import com.ag04.ofx.api.signup.SignupMessageSetResponse;
import com.ag04.ofx.api.bank.BankMessageSetResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.signon.SignOnMessageSetResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("OFX")
@XmlRootElement(name="OFX")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfxResponse implements Serializable {

	@XStreamAlias("SIGNONMSGSRSV1")
	@XmlElement(name="SIGNONMSGSRSV1")
	private SignOnMessageSetResponse signOnMessageSetResponse;
	
	@XStreamAlias("SIGNUPMSGSRSV1")
	@XmlElement(name="SIGNUPMSGSRSV1")
	private SignupMessageSetResponse signupMessageSetResponse;
	
	@XStreamAlias("BANKMSGSRSV1")
	@XmlElement(name="BANKMSGSRSV1")
	private BankMessageSetResponse bankMessageSetResponse;

	@XStreamAlias("CREDITCARDMSGSRSV1")
	@XmlElement(name="CREDITCARDMSGSRSV1")
	private CreditCardMessageSetResponse ccMessageSetResponse;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("signOnMessageSetResponse", signOnMessageSetResponse);
		sbuilder.append("signupMessageSetResponse", signupMessageSetResponse);
		sbuilder.append("bankMessageSetResponse", bankMessageSetResponse);
		sbuilder.append("ccMessageSetResponse", ccMessageSetResponse);
		return sbuilder.toString();
	}

	public List<TransactionWrapperResponse> getAllTrxResponses() {
		List<TransactionWrapperResponse> wrappers = new ArrayList<TransactionWrapperResponse>();
		wrappers.addAll(signupMessageSetResponse.getAllTrxResponses());
		wrappers.addAll(bankMessageSetResponse.getAllTrxResponses());
		wrappers.addAll(ccMessageSetResponse.getAllTrxResponses());
		return wrappers;
	}

	//--- set / get methods ---------------------------------------------------
	
	public SignOnMessageSetResponse getSignOnMessageSetResponse() {
		return signOnMessageSetResponse;
	}

	public void setSignOnMessageSetResponse(SignOnMessageSetResponse signOnMessageSetResponse) {
		this.signOnMessageSetResponse = signOnMessageSetResponse;
	}

	public SignupMessageSetResponse getSignupMessageSetResponse() {
		return signupMessageSetResponse;
	}

	public void setSignupMessageSetResponse(SignupMessageSetResponse signupMessageSetResponse) {
		this.signupMessageSetResponse = signupMessageSetResponse;
	}

	public BankMessageSetResponse getBankMessageSetResponse() {
		return bankMessageSetResponse;
	}

	public void setBankMessageSetResponse(BankMessageSetResponse bankMessageSetResponse) {
		this.bankMessageSetResponse = bankMessageSetResponse;
	}

	public CreditCardMessageSetResponse getCcMessageSetResponse() {
		return ccMessageSetResponse;
	}

	public void setCcMessageSetResponse(CreditCardMessageSetResponse ccMessageSetResponse) {
		this.ccMessageSetResponse = ccMessageSetResponse;
	}
}
