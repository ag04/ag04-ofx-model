package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.OfxStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.TransactionWrapperResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("STMTTRNRS")
@XmlRootElement(name="STMTTRNRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementTrxResponse extends TransactionWrapperResponse {

	@XStreamAlias("STMTRS")
	@XmlElement(name="STMTRS")
	private StatementResponse response;

	public StatementTrxResponse() {
		//
	}

	public StatementTrxResponse(StatementTrxRequest request) {
		super(request);
	}

	public StatementTrxResponse(OfxStatus status, StatementTrxRequest request) {
		super(status, request);
	}

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("response", response);
		return sbuilder.toString();
	}

	@Override
	public String ofxElementName() {
		return "<STMTTRNRS>";
	}

	//--- set / get methods ---------------------------------------------------
	
	public StatementResponse getResponse() {
		return response;
	}

	public void setResponse(StatementResponse response) {
		this.response = response;
	}
	
}
