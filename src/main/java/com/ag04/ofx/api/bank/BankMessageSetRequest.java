package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.OfxRequestMessageSetAggregate;
import com.ag04.ofx.api.TransactionWrapperRequest;
import com.ag04.ofx.api.bank.StatementClosingTrxRequest;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.bank.StatementTrxRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("BANKMSGSRQV1")
@XmlRootElement(name="BANKMSGSRQV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankMessageSetRequest implements OfxRequestMessageSetAggregate, Serializable {

	@XStreamImplicit(itemFieldName="STMTTRNRQ")
	@XmlElement(name="STMTTRNRQ")
	private List<StatementTrxRequest> statementTransactionRequests;

	@XStreamAlias("STMTENDTRNRQ")
	@XmlElement(name="STMTENDTRNRQ")
	private StatementClosingTrxRequest statementClosingTrxRequest;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("statementTransactionRequest", statementTransactionRequests);
		sbuilder.append("statementClosingTrxRequest", statementClosingTrxRequest);
		return sbuilder.toString();
	}

	/**
	 * Returns all (not null) available TransactionRequestWrappers in this message set.
	 *
	 * @return
	 */
	@Override
	public List<TransactionWrapperRequest> getAllTrxRequests() {
		List<TransactionWrapperRequest> wrappers = new ArrayList<TransactionWrapperRequest>();
		if (statementTransactionRequests != null) {
			wrappers.addAll(statementTransactionRequests);
		}
		if (statementClosingTrxRequest != null) {
			wrappers.add(statementClosingTrxRequest);
		}
		return wrappers;
	}

	public void addStatementTransactionRequest(StatementTrxRequest statementTrxRequest) {
		if (statementTransactionRequests == null) {
			statementTransactionRequests = new ArrayList<StatementTrxRequest>();
		}
		statementTransactionRequests.add(statementTrxRequest);
	}

	//--- set / get methods ---------------------------------------------------

	public List<StatementTrxRequest> getStatementTransactionRequests() {
		return statementTransactionRequests;
	}

	public void setStatementTransactionRequests(List<StatementTrxRequest> statementTransactionRequests) {
		this.statementTransactionRequests = statementTransactionRequests;
	}

	public StatementClosingTrxRequest getStatementClosingTrxRequest() {
		return statementClosingTrxRequest;
	}

	public void setStatementClosingTrxRequest(StatementClosingTrxRequest statementClosingTrxRequest) {
		this.statementClosingTrxRequest = statementClosingTrxRequest;
	}
}
