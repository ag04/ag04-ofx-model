package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperRequest;
import com.ag04.ofx.api.TransactionWrapperResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("STMTENDTRNRQ")
@XmlRootElement(name="STMTENDTRNRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementClosingTrxRequest extends TransactionWrapperRequest {

	@XStreamAlias("STMTENDRQ")
	@XmlElement(name="STMTENDRQ")
	private StatementClosingRequest statementClosingRequest;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("statementClosingRequest", statementClosingRequest);
		return sbuilder.toString();
	}

	@Override
	public TransactionWrapperResponse buildResponse(OfxStatus ofxStatus) {
		if (ofxStatus == null) {
			return new StatementClosingTrxResponse(this);
		}
		return new StatementClosingTrxResponse(ofxStatus, this);
	}

	@Override
	public String ofxElementName() {
		return "<STMTENDTRNRQ>";
	}

	//--- set / get methods ---------------------------------------------------
	
	public StatementClosingRequest getStatementClosingRequest() {
		return statementClosingRequest;
	}

	public void setStatementClosingRequest(StatementClosingRequest statementClosingRequest) {
		this.statementClosingRequest = statementClosingRequest;
	}
	
	
	
	
}
