package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * You can use the <STMTENDRQ> …<STMTENDRS> request and response pair to download statement closing information for checking, savings, money market, CD, and line of credit accounts.
 *
 * Section 11.5.3 describes download for credit card accounts.
 *
 * @author dmadunic
 *
 */
@XStreamAlias("STMTENDRQ")
@XmlRootElement(name="STMTENDRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementClosingRequest {

    @XStreamAlias("BANKACCTFROM")
    @XmlElement(name="BANKACCTFROM")
    private BankAccountFromAggregate accountAggregate;


    /**
     * Start date of statement requested, datetime
     */
    @XStreamAlias("DTSTART")
    @XmlElement(name="DTSTART")
    private DateTime startDate;

    /**
     * End date of statement requested, datetime
     */
    @XStreamAlias("DTEND")
    @XmlElement(name="DTEND")
    private DateTime endDate;


    @XStreamAlias("INCSTMTIMG")
    @XmlElement(name="INCSTMTIMG")
    private Boolean includeStatementImages;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("accountAggregate", accountAggregate);
        sbuilder.append("startDate", startDate);
        sbuilder.append("endDate", endDate);
        sbuilder.append("includeStatementImages", includeStatementImages);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------


    public BankAccountFromAggregate getAccountAggregate() {
        return accountAggregate;
    }

    public void setAccountAggregate(BankAccountFromAggregate accountAggregate) {
        this.accountAggregate = accountAggregate;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public Boolean getIncludeStatementImages() {
        return includeStatementImages;
    }

    public void setIncludeStatementImages(Boolean includeStatementImages) {
        this.includeStatementImages = includeStatementImages;
    }

}
