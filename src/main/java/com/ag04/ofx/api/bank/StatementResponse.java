package com.ag04.ofx.api.bank;

import java.util.List;

import javax.xml.bind.annotation.*;

import com.ag04.ofx.api.statement.BaseStatementResponse;
import com.ag04.ofx.api.statement.TransactionsDataAggregate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.common.BalanceAggregate;
import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.ag04.ofx.api.statement.AvailableBalanceAggregate;
import com.ag04.ofx.api.statement.LedgerBalanceAggregate;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
 * @author dmaudnic
 *
 */
@XStreamAlias("STMTRS")
@XmlRootElement(name="STMTRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementResponse extends BaseStatementResponse {

	/**
	 * Account-from aggregate, see section 11.3.1 / M
	 */
	@XStreamAlias("BANKACCTFROM")
	@XmlElement(name="BANKACCTFROM")
	private BankAccountFromAggregate bankAccountAggregate;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("bankAccountAggregate", bankAccountAggregate);
		return sbuilder.toString();
	}

	//--- set / get methods ---------------------------------------------------

	public BankAccountFromAggregate getBankAccountAggregate() {
		return bankAccountAggregate;
	}

	public void setBankAccountAggregate(
			BankAccountFromAggregate bankAccountAggregate) {
		this.bankAccountAggregate = bankAccountAggregate;
	}

}
