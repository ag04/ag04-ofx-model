package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by domagoj on 27/05/16.
 */
@XStreamAlias("STMTENDTRNRS")
@XmlRootElement(name="STMTENDTRNRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementClosingTrxResponse extends TransactionWrapperResponse {

    @XStreamAlias("STMTENDRS")
    @XmlElement(name="STMTENDRS")
    private StatementClosingResponse statementClosingResponse;


    public StatementClosingTrxResponse() {
        //
    }

    public StatementClosingTrxResponse(StatementClosingTrxRequest request) {
        super(request);
    }

    public StatementClosingTrxResponse(OfxStatus status, StatementClosingTrxRequest request) {
        super(status, request);
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("statementClosingResponse", statementClosingResponse);
        return sbuilder.toString();
    }

    @Override
    public String ofxElementName() {
        return "<STMTENDTRNRS>";
    }

    //--- set / get methods ---------------------------------------------------


    public StatementClosingResponse getStatementClosingResponse() {
        return statementClosingResponse;
    }

    public void setStatementClosingResponse(StatementClosingResponse statementClosingResponse) {
        this.statementClosingResponse = statementClosingResponse;
    }
}
