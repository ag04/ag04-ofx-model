package com.ag04.ofx.api.bank;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.ag04.ofx.api.statement.IncludeTransactionsAggregate;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("STMTRQ")
@XmlRootElement(name="STMTRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementRequest {

	@XStreamAlias("BANKACCTFROM")
	@XmlElement(name="BANKACCTFROM")
	private BankAccountFromAggregate accountAggregate;

	@XStreamAlias("INCTRAN")
	@XmlElement(name="INCTRAN")
	private IncludeTransactionsAggregate includeTransactions;
	
	@XStreamAlias("INCTRANIMG")
	@XmlElement(name="INCTRANIMG")
	private Boolean includeTrxImage;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("accountAggregate", accountAggregate);
		sbuilder.append("includeTransactions", includeTransactions);
		sbuilder.append("includeTrxImage", includeTrxImage);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	public BankAccountFromAggregate getAccountAggregate() {
		return accountAggregate;
	}

	public void setAccountAggregate(BankAccountFromAggregate accountAggregate) {
		this.accountAggregate = accountAggregate;
	}

	public IncludeTransactionsAggregate getIncludeTransactions() {
		return includeTransactions;
	}

	public void setIncludeTransactions(
			IncludeTransactionsAggregate includeTransactions) {
		this.includeTransactions = includeTransactions;
	}

	public Boolean getIncludeTrxImage() {
		return includeTrxImage;
	}

	public void setIncludeTrxImage(Boolean includeTrxImage) {
		this.includeTrxImage = includeTrxImage;
	}

	
}
