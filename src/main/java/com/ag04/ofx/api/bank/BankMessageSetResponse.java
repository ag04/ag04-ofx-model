package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.OfxResponseMessageSetAggregate;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.ag04.ofx.api.UnsupportedTransactioWrapper;
import com.ag04.ofx.api.signup.AccountInfoTrxResponse;
import com.ag04.ofx.api.user.EnrollmentTrxResponse;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.*;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("BANKMSGSRSV1")
@XmlRootElement(name="BANKMSGSRSV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class BankMessageSetResponse implements Serializable, OfxResponseMessageSetAggregate {
	public static final Class[] SUPPORTED_TRANSACTIONS = new Class[] { StatementClosingTrxResponse.class, StatementTrxResponse.class };

	public static Set<Class> supportedTransactions = new HashSet<Class>(Arrays.asList(SUPPORTED_TRANSACTIONS));

	@XStreamImplicit(itemFieldName="STMTTRNRS")
	@XmlElement(name="STMTTRNRS")
	private List<StatementTrxResponse> statementTransactionResponses;


	@XStreamAlias("STMTENDTRNRS")
	@XmlElement(name="STMTENDTRNRS")
	private StatementClosingTrxResponse statementClosingTrxResponse;

	public static boolean belongsTo(Class clazz) {
		return supportedTransactions.contains(clazz);
	}

	public void setTrxResponse(TransactionWrapperResponse response) {
		if (response instanceof StatementTrxResponse) {
			addStatementTransactionResponse((StatementTrxResponse) response);
		} else if (response instanceof StatementClosingTrxResponse) {
			statementClosingTrxResponse = (StatementClosingTrxResponse) response;
		} else {
			throw new UnsupportedTransactioWrapper(response);
		}
	}

	public void addStatementTransactionResponse(StatementTrxResponse statementTrxResponse) {
		if (statementTransactionResponses == null) {
			statementTransactionResponses = new ArrayList<StatementTrxResponse>();
		}
		statementTransactionResponses.add(statementTrxResponse);
	}

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("statementTransactionResponses", statementTransactionResponses);
		sbuilder.append("statementClosingTrxRequest", statementClosingTrxResponse);
		return sbuilder.toString();
	}

	//--- interface methods ---------------------------------------------------

	/**
	 * Returns all (not null) available TransactionResponseWrappers in this message set.
	 *
	 * @return
	 */
	@Override
	public List<TransactionWrapperResponse> getAllTrxResponses() {
		List<TransactionWrapperResponse> wrappers = new ArrayList<TransactionWrapperResponse>();
		if (statementTransactionResponses != null) {
			wrappers.addAll(statementTransactionResponses);
		}
		if (statementClosingTrxResponse != null) {
			wrappers.add(statementClosingTrxResponse);
		}
		return wrappers;
	}

	//--- set / get methods ---------------------------------------------------


	public List<StatementTrxResponse> getStatementTransactionResponses() {
		return statementTransactionResponses;
	}

	public void setStatementTransactionResponses(List<StatementTrxResponse> statementTransactionResponses) {
		this.statementTransactionResponses = statementTransactionResponses;
	}

	public StatementClosingTrxResponse getStatementClosingTrxResponse() {
		return statementClosingTrxResponse;
	}

	public void setStatementClosingTrxResponse(StatementClosingTrxResponse statementClosingTrxResponse) {
		this.statementClosingTrxResponse = statementClosingTrxResponse;
	}


}
