package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.TransactionWrapperRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("STMTTRNRQ")
@XmlRootElement(name="STMTTRNRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementTrxRequest extends TransactionWrapperRequest {

	@XStreamAlias("STMTRQ")
	@XmlElement(name="STMTRQ")
	private StatementRequest statementRequest;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("statementRequest", statementRequest);
		return sbuilder.toString();
	}

	@Override
	public TransactionWrapperResponse buildResponse(OfxStatus ofxStatus) {
		if (ofxStatus == null) {
			return new StatementTrxResponse(this);
		}
		return new StatementTrxResponse(ofxStatus, this);
	}

	@Override
	public String ofxElementName() {
		return "<STMTTRNRQ>";
	}

	//--- set / get methods ---------------------------------------------------

	public StatementRequest getStatementRequest() {
		return statementRequest;
	}

	public void setStatementRequest(StatementRequest statementRequest) {
		this.statementRequest = statementRequest;
	}
}
