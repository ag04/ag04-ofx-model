package com.ag04.ofx.api.bank;

import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.ag04.ofx.api.statement.ClosingStatement;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * You can use the <STMTENDRQ> …<STMTENDRS> request and response pair to download statement closing information for checking, savings, money market, CD, and line of credit accounts.
 *
 * Section 11.5.3 describes download for credit card accounts.
 *
 * Created by dmadunic on 27/05/16.
 */
@XStreamAlias("STMTENDRS")
@XmlRootElement(name="STMTENDRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementClosingResponse {

    /**
     * Default currency for the statement, currsymbol / M
     */
    @XStreamAlias("CURDEF")
    @XmlElement(name="CURDEF")
    private String currency;

    /**
     * Account-from aggregate, see section 11.3.1 / M
     */
    @XStreamAlias("BANKACCTFROM")
    @XmlElement(name="BANKACCTFROM")
    private BankAccountFromAggregate bankAccountAggregate;

    @XStreamAlias("CLOSING")
    @XmlElement(name="CLOSING")
    List<ClosingStatement> closingStatements;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("currency", currency);
        sbuilder.append("bankAccountAggregate", bankAccountAggregate);
        if (closingStatements != null) {
            sbuilder.append("closingStatements", closingStatements.size());
        }
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BankAccountFromAggregate getBankAccountAggregate() {
        return bankAccountAggregate;
    }

    public void setBankAccountAggregate(BankAccountFromAggregate bankAccountAggregate) {
        this.bankAccountAggregate = bankAccountAggregate;
    }

    public List<ClosingStatement> getClosingStatements() {
        return closingStatements;
    }

    public void setClosingStatements(List<ClosingStatement> closingStatements) {
        this.closingStatements = closingStatements;
    }

}
