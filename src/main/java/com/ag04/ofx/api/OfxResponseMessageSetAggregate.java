package com.ag04.ofx.api;

import java.util.List;

/**
 * Base interface representing Ofx Response Message set aggregate.
 *
 * Created by domagoj on 28/05/16.
 */
public interface OfxResponseMessageSetAggregate {
    /**
     * Returns all (not null) available TransactionResponseWrappers in this message set.
     *
     * @return
     */
    List<TransactionWrapperResponse> getAllTrxResponses();
}
