package com.ag04.ofx.api;

/**
 * Created by domagoj on 27/05/16.
 */
public enum OfxStatusCode {
    SUCCES(0L, "Success", OfxSeverity.INFO),
    GENERAL_ERROR(2000L, "General error", OfxSeverity.ERROR),
    FURTHER_AUTH_REQ(3000L, "User credentials are correct, but further authentication required", OfxSeverity.ERROR),
    MFACHALLENGEANSWER(3001L, "MFACHALLENGEANSWER contains invalid information", OfxSeverity.ERROR),
    FI_MISSING_OR_INVALID(13504L, "<FI> Missing or Invalid in <SONRQ>", OfxSeverity.ERROR),
    SERVER_MAINTENANCE(13505L, "Server undergoing maintenance; try again later", OfxSeverity.ERROR),
    USERPASS_CHANGE(15000L, "Must change USERPASS", OfxSeverity.INFO),
    ACCOUNT_IN_USE(15501L, "Customer account already in use", OfxSeverity.ERROR),
    USERPASS_LOCKOUT(15502L, "USERPASS Lockout", OfxSeverity.ERROR),
    EMPTY_SIGNON(15506L, "Empty signon transaction not supported", OfxSeverity.ERROR),
    INVALID_SIGNON_PIN(15507L, "Signon invalid without supporting pin change request", OfxSeverity.ERROR),
    CLIENTUID_ERROR(15510L, "CLIENTUID error", OfxSeverity.ERROR),
    CONTACT_FI_ERROR(15511L, "User should contact financial institution", OfxSeverity.ERROR),
    AUTHTOKEN_REQUIRED(15512L, "OFX server requires AUTHTOKEN in signon during the next session", OfxSeverity.ERROR),
    AUTHTOKEN_INVALID(15513L, "AUTHTOKEN invalid", OfxSeverity.ERROR),
    ACCESSTOKEN_REQUIRED(15514L, "OFX Server requires ACCESSTOKEN for authentication", OfxSeverity.ERROR),
    ACCESSTOKEN_AUTH_FAILED(15515L, "Authentication failed; ACCESSTOKEN provided is invalid/unrecognized by the server", OfxSeverity.ERROR),
    ACCESSTOKEN_EXIPRED(15516L, "ACCESSTOKEN provided is expired and needs refresh", OfxSeverity.ERROR),
    SIGNION_INVALID(15500L, "Signon invalid (see section 2.5.1)", OfxSeverity.ERROR);

    OfxStatusCode(Long code, String message, OfxSeverity severity) {
        this.code = code;
        this.message = message;
        this.severity = severity;
    }

    private Long code;
    private String message;
    private OfxSeverity severity;

    public Long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public OfxSeverity getSeverity() {
        return severity;
    }
}
