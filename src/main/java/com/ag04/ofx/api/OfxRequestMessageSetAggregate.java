package com.ag04.ofx.api;

import java.util.List;

/**
 * Base interface representing Ofx Request Message set aggregate.
 *
 * Created by dmadunic on 28/05/16.
 */
public interface OfxRequestMessageSetAggregate {

    /**
     * Returns all (not null) available TransactionRequestWrappers in this message set.
     *
     * @return
     */
    List<TransactionWrapperRequest> getAllTrxRequests();

}
