package com.ag04.ofx.api;

/**
 * Created by dmadunic on 29/05/16.
 */
public class UnsupportedTransactioWrapper extends RuntimeException {

    private TransactionWrapper wrapper;

    public UnsupportedTransactioWrapper(TransactionWrapper wrapper) {
        this.wrapper = wrapper;
    }

}
