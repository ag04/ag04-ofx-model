package com.ag04.ofx.api.statement;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.ag04.ofx.api.statement.StatementTransaction;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("BANKTRANLIST")
@XmlRootElement(name="BANKTRANLIST")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionsDataAggregate {

	public TransactionsDataAggregate() {
		//
	}

	public TransactionsDataAggregate(DateTime startDate, DateTime endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}
	/**
	 * Start date for transaction data, date
	 */
	@XStreamAlias("DTSTART")
	@XmlElement(name="DTSTART")
	private DateTime startDate;
	/**
	 * Value that client should send in next <DTSTART> request to ensure that it does not miss any transactions, date
	 */
	@XStreamAlias("DTEND")
	@XmlElement(name="DTEND")
	private DateTime endDate;
	
	/**
	 * List of statement transaction (0 or more), see section 11.4.3 
	 */
	@XStreamAlias("STMTTRN")
	@XmlElement(name="STMTTRN")
	private List<StatementTransaction> transactions;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("startDate", startDate);
		sbuilder.append("endDate", endDate);
		sbuilder.append("transactions", transactions);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	
	public DateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}

	public DateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	public List<StatementTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<StatementTransaction> transactions) {
		this.transactions = transactions;
	}
	
}
