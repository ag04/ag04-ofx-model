package com.ag04.ofx.api.statement;
/**
 * See Ofx specificaton p. 212.
 * 
 * @author dmadunic
 *
 */
public enum TransactionType {

	CREDIT, DEBIT, INT, DIV, FEE, SRVCHG, DEP, ATM, POS, XFER, CHECK, PAYMENT, CASH, DIRECTDEP, DIRECTDEBIT, REPEATPMT, OTHER;
}
