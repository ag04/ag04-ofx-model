package com.ag04.ofx.api.statement;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.joda.time.LocalDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Checking, savings, money market, CD, or credit line account uses the <CLOSING> aggregate to describe statement closing information.
 *
 * The <FITID> provides a way for the client to distinguish one closing statement from another.
 *
 * For each <CLOSING> aggregate returned, clients can retrieve corresponding transactions by using <DTPOSTSTART> and <DTPOSTEND> as <DTSTART> and <DTEND> in a <STMTRQ> request.
 *
 * Created by dmadunic on 01/06/16.
 */
@XStreamAlias("CLOSING")
@XmlRootElement(name="CLOSING")
@XmlAccessorType(XmlAccessType.FIELD)
public class ClosingStatement implements Serializable {

    /**
     * Unique identifier for this statement, FITID [Mandatory]
     */
    @XStreamAlias("FITID")
    @XmlElement(name="FITID")
    private String id;


    /**
     * Opening statement date, date
      */
    @XStreamAlias("DTOPEN")
    @XmlElement(name="DTOPEN")
    private LocalDate openDate;

    /**
     * Closing statement date, date [Mandatory]
     */
    @XStreamAlias("DTCLOSE")
    @XmlElement(name="DTCLOSE")
    private LocalDate closeDate;

    /**
     * Closing date of next statement, date
     */
    @XStreamAlias("DTNEXT")
    @XmlElement(name="DTNEXT")
    private LocalDate nextCloseDate;

    /**
     * Opening statement balance, amount
     */
    @XStreamAlias("BALOPEN")
    @XmlElement(name="BALOPEN")
    private BigDecimal openBalance;

    /**
     * Closing statement balance, amount [Mandatory]
     */
    @XStreamAlias("BALCLOSE")
    @XmlElement(name="BALCLOSE")
    private BigDecimal closeBalance;

    /**
     * Minimum balance in statement period, amount
     */
    @XStreamAlias("BALMIN")
    @XmlElement(name="BALMIN")
    private BigDecimal minimumBalance;

    //TODO: add <DEPANDCREDIT> Total of deposits and credits, including interest, amount
    //TODO: add <CHKANDDEB> Total of checks and debits, including fees, amount
    //TODO: add <TOTALFEES> Total of all fees, amount
    //TODO: add <TOTALINT> Total of all interest, amount
    //TODO: add <INTRATE> Effective interest rate, taking into account any changes in rates which applied during this statement period, rate
    //TODO: add <INTYTD> Year-to-date interest paid on the account, amount

    /**
     * Start date of transaction data for this statement, date
     * A client should be able to use this date in a <STMTRQ> to request transactions that match this statement.
     *
     * [Mandatory]
     *
     */
    @XStreamAlias("DTPOSTSTART")
    @XmlElement(name="DTPOSTSTART")
    private LocalDate trxDateStart;

    /**
     * End date of transaction data for this statement, date
     * A client should be able to use this date in a <STMTRQ> to request transactions that match this statement.
     *
     * [Mandatory]
     *
     */
    @XStreamAlias("DTPOSTEND")
    @XmlElement(name="DTPOSTEND")
    private LocalDate trxDateEnd;

    //TODO: add <CREDITLINEINFO></CREDITLINEINFO> aggregate ...

    /**
     * Marketing information (at most 1), A-360
     */
    @XStreamAlias("MKTGINFO")
    @XmlElement(name="MKTGINFO")
    private String marketingInfo;

    //TODO: add <IMAGEDATA></IMAGEDATA> Image data aggregate, see section 3.1.6.1
    //TODO: add <CURRENCY></CURRENCY> Currency, if different from CURDEF, see section 5.2.
    //TODO: add <ORIGCURRENCY></ORIGCURRENCY> Currency, if different from CURDEF, see section 5.2.

    //--- set / get methods ---------------------------------------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getOpenDate() {
        return openDate;
    }

    public void setOpenDate(LocalDate openDate) {
        this.openDate = openDate;
    }

    public LocalDate getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(LocalDate closeDate) {
        this.closeDate = closeDate;
    }

    public LocalDate getNextCloseDate() {
        return nextCloseDate;
    }

    public void setNextCloseDate(LocalDate nextCloseDate) {
        this.nextCloseDate = nextCloseDate;
    }

    public BigDecimal getOpenBalance() {
        return openBalance;
    }

    public void setOpenBalance(BigDecimal openBalance) {
        this.openBalance = openBalance;
    }

    public BigDecimal getCloseBalance() {
        return closeBalance;
    }

    public void setCloseBalance(BigDecimal closeBalance) {
        this.closeBalance = closeBalance;
    }

    public BigDecimal getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(BigDecimal minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public LocalDate getTrxDateStart() {
        return trxDateStart;
    }

    public void setTrxDateStart(LocalDate trxDateStart) {
        this.trxDateStart = trxDateStart;
    }

    public LocalDate getTrxDateEnd() {
        return trxDateEnd;
    }

    public void setTrxDateEnd(LocalDate trxDateEnd) {
        this.trxDateEnd = trxDateEnd;
    }

    public String getMarketingInfo() {
        return marketingInfo;
    }

    public void setMarketingInfo(String marketingInfo) {
        this.marketingInfo = marketingInfo;
    }
}
