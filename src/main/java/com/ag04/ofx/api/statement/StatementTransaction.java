package com.ag04.ofx.api.statement;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.ag04.ofx.api.account.bank.BankAccountToAggregate;
import com.ag04.ofx.api.account.creditcard.CreditCardAccountToAggregate;
import com.ag04.ofx.api.common.Currency;
import com.ag04.ofx.api.common.OriginalCurrency;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Statement-transaction aggregate.<br>
 * <br>
 * A <STMTTRN> aggregate describes a single transaction. It identifies the type of the transaction and the date it was posted. The aggregate can also provide additional information to help the customer recognize the transaction: check number, payee name, and memo. The transaction can have a Standard Industrial Code that a client can use to categorize the transaction.
 * Each <STMTTRN> contains an <FITID> that the client uses to detect whether the server has previously downloaded the transaction.
 * Transaction amounts are signed from the perspective of the customer. For example, a credit card payment is positive while a credit card purchase is negative.
 * NOT supported elements:
 * <ul>
 * <li>MEMO</li>
 * <li>IMAGEDATA</li>
 * <li>INV401KSOURCE</li>
 * </ul>
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("STMTTRN")
@XmlRootElement(name="STMTTRN")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatementTransaction {

	/**
	 * Transaction type, see section 11.4.3.1 for possible values. This element does not change the effect of the transaction upon the balance (increases and decreases are indicated by the sign of the <TRNAMT>)
	 */
	@XStreamAlias("TRNTYPE")
	@XmlElement(name="TRNTYPE")
	private TransactionType type;
	
	/**
	 * Date transaction was posted to account, datetime
	 */
	@XStreamAlias("DTPOSTED")
	@XmlElement(name="DTPOSTED")
	private DateTime postedDate;
	
	/**
	 * Date user initiated transaction, if known, datetime
	 */
	@XStreamAlias("DTUSER")
	@XmlElement(name="DTUSER")
	private DateTime userInitiatedDate;
	/**
	 * Date funds are available (value date), datetime
	 */
	@XStreamAlias("DTAVAIL")
	@XmlElement(name="DTAVAIL")
	private DateTime valueDate;
	
	/**
	 * Amount of transaction, amount
	 */
	@XStreamAlias("TRNAMT")
	@XmlElement(name="TRNAMT")
	private BigDecimal amount;
	
	/**
	 * Transaction ID issued by financial institution.
	 * Used to detect duplicate downloads, FITID
	 */
	@XStreamAlias("FITID")
	@XmlElement(name="FITID")
	private String fiTrxId;
	
	/**
	 * If present, the FITID of a previously sent transaction that is corrected by this record. This transaction replaces or deletes the transaction that it corrects, based on the value of <CORRECTACTION> below, FITID
	 */
	@XStreamAlias("CORRECTFITID")
	@XmlElement(name="CORRECTFITID")
	private String correctedFiTrxId;
	
	/**
	 * Actions can be REPLACE or DELETE. REPLACE replaces the transaction referenced by CORRECTFITID; DELETE deletes it.
	 */
	@XStreamAlias("CORRECTACTION")
	@XmlElement(name="CORRECTACTION")
	private CorrectionAction correctionAction;
	
	/**
	 * Server assigned transaction ID; used for transactions initiated by client, such as payment or funds transfer. If a transaction associated with a transfer contains a SRVRTID, the associated account is either the transfer's "to" or "from" account. Transactions for both accounts in a transfer would contain the same SRVRTID. SRVRTID
	 * @param other
	 */
	@XStreamAlias("SRVRTID")
	@XmlElement(name="SRVRTID")
	private String serverTrxId;
	
	/**
	 * Check (or other reference) number, A-12
	 */
	@XStreamAlias("CHECKNUM")
	@XmlElement(name="CHECKNUM")
	private String checkNumber;
	
	/**
	 * Reference number that uniquely identifies the transaction. Can be used in addition to or instead of a <CHECKNUM>, A-32
	 */
	@XStreamAlias("REFNUM")
	@XmlElement(name="REFNUM")
	private String referenceNumber;
	
	/**
	 * Standard Industrial Code, N-6
	 */
	@XStreamAlias("SIC")
	@XmlElement(name="SIC")
	private String sic;
	
	/**
	 * Payee identifier if available, A-12
	 */
	@XStreamAlias("PAYEEID")
	@XmlElement(name="PAYEEID")
	private String payeeId;
	
	//--- PAYEE BLOCK ---------------------------------------------------------
	
	/**
	 * Name of payee or description of transaction, A-32
	 * Note:Provide NAME or PAYEE, not both
	 */
	@XStreamAlias("NAME")
	@XmlElement(name="NAME")
	private String payeeName;

	/**
	 * Extended name of payee or description of transaction, A-100
	 */
	@XStreamAlias("EXTDNAME")
	@XmlElement(name="EXTDNAME")
	private String payeeExtendedName;
	
	//--- ACCOUNTTO BLOCK -----------------------------------------------------

	/**
	 * If this was a transfer to an account and the account information is available, see section 11.3.1
	 */
	@XStreamAlias("BANKACCTTO")
	@XmlElement(name="BANKACCTTO")
	private BankAccountToAggregate bankAccountToAggregate;
	
	@XStreamAlias("CCACCTTO")
	@XmlElement(name="CCACCTTO")
	private CreditCardAccountToAggregate creditCardAccountToAggregate;
	
	//--- NOT SUPPORTED -------------------------------------------------------
	
	/**
	 * Extra information (not in <NAME>), MEMO
	 */
	// MEMO
	/**
	 * 
	 */
	// IMAGEDATA
	
	//@XStreamAlias("INV401KSOURCE")
	//public CashSource source;
	
	//--- CURRENCY OPTIONS BLOCK ----------------------------------------------
	
	/**
	 * Currency, if different from CURDEF, see section 5.2.
	 * 
	 * Currency options. Choose either <CURRENCY> or <ORIGCURRENCY>.
	 */
	@XStreamAlias("CURRENCY")
	@XmlElement(name="CURRENCY")
	private Currency currency;
	
	@XStreamAlias("ORIGCURRENCY")
	@XmlElement(name="ORIGCURRENCY")
	private OriginalCurrency originalCurrency;
	
	
	//--- AG04 add on ----------------------------------------------------------
	
	@XStreamAlias("DESCRIPTION")
	@XmlElement(name="DESCRIPTION")
	private String description;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("type", type);
		sbuilder.append("postedDate", postedDate);
		sbuilder.append("userInitiatedDate", userInitiatedDate);
		sbuilder.append("valueDate", valueDate);
		sbuilder.append("amount", amount);
		sbuilder.append("fiTrxId", fiTrxId);
		sbuilder.append("correctedFiTrxId", correctedFiTrxId);
		sbuilder.append("correctionAction", correctionAction);
		sbuilder.append("serverTrxId", serverTrxId);
		sbuilder.append("checkNumber", checkNumber);
		sbuilder.append("referenceNumber", referenceNumber);
		sbuilder.append("sic", sic);
		sbuilder.append("payeeId", payeeId);
		sbuilder.append("payeeName", payeeName);
		sbuilder.append("payeeExtendedName", payeeExtendedName);
		sbuilder.append("description", description);
		sbuilder.append("bankAccountToAggregate", bankAccountToAggregate);
		sbuilder.append("creditCardAccountToAggregate", creditCardAccountToAggregate);
		sbuilder.append("currency", currency);
		sbuilder.append("originalCurrency", originalCurrency);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	
	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public DateTime getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(DateTime postedDate) {
		this.postedDate = postedDate;
	}

	public DateTime getUserInitiatedDate() {
		return userInitiatedDate;
	}

	public void setUserInitiatedDate(DateTime userInitiatedDate) {
		this.userInitiatedDate = userInitiatedDate;
	}

	public DateTime getValueDate() {
		return valueDate;
	}

	public void setValueDate(DateTime valueDate) {
		this.valueDate = valueDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getFiTrxId() {
		return fiTrxId;
	}

	public void setFiTrxId(String fiTrxId) {
		this.fiTrxId = fiTrxId;
	}

	public String getCorrectedFiTrxId() {
		return correctedFiTrxId;
	}

	public void setCorrectedFiTrxId(String correctedFiTrxId) {
		this.correctedFiTrxId = correctedFiTrxId;
	}

	public CorrectionAction getCorrectionAction() {
		return correctionAction;
	}

	public void setCorrectionAction(CorrectionAction correctionAction) {
		this.correctionAction = correctionAction;
	}

	public String getServerTrxId() {
		return serverTrxId;
	}

	public void setServerTrxId(String serverTrxId) {
		this.serverTrxId = serverTrxId;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getSic() {
		return sic;
	}

	public void setSic(String sic) {
		this.sic = sic;
	}

	public String getPayeeId() {
		return payeeId;
	}

	public void setPayeeId(String payeeId) {
		this.payeeId = payeeId;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getPayeeExtendedName() {
		return payeeExtendedName;
	}

	public void setPayeeExtendedName(String payeeExtendedName) {
		this.payeeExtendedName = payeeExtendedName;
	}

	public BankAccountToAggregate getBankAccountToAggregate() {
		return bankAccountToAggregate;
	}

	public void setBankAccountToAggregate(BankAccountToAggregate bankAccountToAggregate) {
		this.bankAccountToAggregate = bankAccountToAggregate;
	}

	public CreditCardAccountToAggregate getCreditCardAccountToAggregate() {
		return creditCardAccountToAggregate;
	}

	public void setCreditCardAccountToAggregate(CreditCardAccountToAggregate creditCardAccountToAggregate) {
		this.creditCardAccountToAggregate = creditCardAccountToAggregate;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public OriginalCurrency getOriginalCurrency() {
		return originalCurrency;
	}

	public void setOriginalCurrency(OriginalCurrency originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
