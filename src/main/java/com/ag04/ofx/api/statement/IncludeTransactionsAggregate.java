package com.ag04.ofx.api.statement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 * 
 */
@XStreamAlias("INCTRAN")
@XmlRootElement(name="INCTRAN")
@XmlAccessorType(XmlAccessType.FIELD)
public class IncludeTransactionsAggregate {

	/**
	 * Start date of statement requested, datetime
	 */
	@XStreamAlias("DTSTART")
	@XmlElement(name="DTSTART")
	private DateTime startDate;
	
	/**
	 * End date of statement requested, datetime 
	 */
	@XStreamAlias("DTEND")
	@XmlElement(name="DTEND")
	private DateTime endDate;
	
	/**
	 * Include transactions flag, Boolean / Mandatory 	
	 */
	@XStreamAlias("INCLUDE")
	@XmlElement(name="INCLUDE")
	private boolean includeTransactions;

	public IncludeTransactionsAggregate() {
		// default constructor ...
	}

	public IncludeTransactionsAggregate(DateTime startDate, DateTime endDate, boolean includeTransactions) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.includeTransactions = includeTransactions;
	}

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("includeTransactions", includeTransactions);
		sbuilder.append("startDate", startDate);
		sbuilder.append("endDate", endDate);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	
	public DateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(DateTime startDate) {
		this.startDate = startDate;
	}

	public DateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	public boolean isIncludeTransactions() {
		return includeTransactions;
	}

	public void setIncludeTransactions(boolean includeTransactions) {
		this.includeTransactions = includeTransactions;
	}
	
}
