package com.ag04.ofx.api.statement;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("LEDGERBAL")
@XmlRootElement(name="LEDGERBAL")
@XmlAccessorType(XmlAccessType.FIELD)
public class LedgerBalanceAggregate extends BalanceAggregateEssence {

}