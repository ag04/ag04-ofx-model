package com.ag04.ofx.api.statement;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XmlRootElement(name="AVAILBAL")
@XStreamAlias("AVAILBAL")
public class AvailableBalanceAggregate extends BalanceAggregateEssence {

}
