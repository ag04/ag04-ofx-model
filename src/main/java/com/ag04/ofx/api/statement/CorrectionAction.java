package com.ag04.ofx.api.statement;
/**
 * 
 * @author dmadunic
 *
 */
public enum CorrectionAction {
	REPLACE, DELETE;
}
