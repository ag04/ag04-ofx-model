package com.ag04.ofx.api.statement;

import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.ag04.ofx.api.common.BalanceAggregate;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

/**
 * Base StatementResponse class used by:
 *
 *  <ul>
 *      <li>(Bank) StatementResponse</li>
 *      <li>CreditCardStatementResponse</li>
 *  </ul>
 *
 * Created by dmadunic on 21/08/2016.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseStatementResponse {

    /**
     * Default currency for the statement, currsymbol / M
     */
    @XStreamAlias("CURDEF")
    @XmlElement(name="CURDEF")
    private String currency;

    /**
     * Statement-transaction-data aggregate
     */
    @XStreamAlias("BANKTRANLIST")
    @XmlElement(name="BANKTRANLIST")
    private TransactionsDataAggregate transactionsData;

    /**
     * Ledger balance aggregate / M
     */
    @XStreamAlias("LEDGERBAL")
    @XmlElement(name="LEDGERBAL")
    private LedgerBalanceAggregate ledgerBalanceAggregate;

    /**
     * Available balance aggregate
     */
    @XStreamAlias("AVAILBAL")
    @XmlElement(name="AVAILBAL")
    private AvailableBalanceAggregate availableBalanceAggregate;

    /**
     * List of miscellaneous other balances
     */
    @XStreamAlias("BALLIST")
    @XmlElementWrapper(name="BALLIST")
    @XmlElement(name="BAL")
    private List<BalanceAggregate> balances;

    /**
     * Marketing information (at most 1), A-360
     */
    @XStreamAlias("MKTGINFO")
    @XmlElement(name="MKTGINFO")
    private String marketingInfo;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("currency", currency);
        sbuilder.append("transactionsData", transactionsData);
        sbuilder.append("ledgerBalanceAggregate", ledgerBalanceAggregate);
        sbuilder.append("availableBalanceAggregate", availableBalanceAggregate);
        sbuilder.append("balances", balances);
        sbuilder.append("marketingInfo", marketingInfo);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public TransactionsDataAggregate getTransactionsData() {
        return transactionsData;
    }

    public void setTransactionsData(TransactionsDataAggregate transactionsData) {
        this.transactionsData = transactionsData;
    }

    public LedgerBalanceAggregate getLedgerBalanceAggregate() {
        return ledgerBalanceAggregate;
    }

    public void setLedgerBalanceAggregate(LedgerBalanceAggregate ledgerBalanceAggregate) {
        this.ledgerBalanceAggregate = ledgerBalanceAggregate;
    }

    public AvailableBalanceAggregate getAvailableBalanceAggregate() {
        return availableBalanceAggregate;
    }

    public void setAvailableBalanceAggregate(AvailableBalanceAggregate availableBalanceAggregate) {
        this.availableBalanceAggregate = availableBalanceAggregate;
    }

    public List<BalanceAggregate> getBalances() {
        return balances;
    }

    public void setBalances(List<BalanceAggregate> balances) {
        this.balances = balances;
    }

    public String getMarketingInfo() {
        return marketingInfo;
    }

    public void setMarketingInfo(String marketingInfo) {
        this.marketingInfo = marketingInfo;
    }
}
