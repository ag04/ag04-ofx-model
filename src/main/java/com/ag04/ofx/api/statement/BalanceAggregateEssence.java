package com.ag04.ofx.api.statement;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author dmadunic
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BalanceAggregateEssence {
	
	@XStreamAlias("BALAMT")
	@XmlElement(name="BALAMT")
	private BigDecimal amount;
	
	@XStreamAlias("DTASOF")
	@XmlElement(name="DTASOF")
	private DateTime date;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("amount", amount);
		sbuilder.append("date", date);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

}
