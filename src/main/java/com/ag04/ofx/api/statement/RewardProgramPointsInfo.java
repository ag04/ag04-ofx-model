package com.ag04.ofx.api.statement;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XStreamAlias("REWARDINFO")
@XmlRootElement(name="REWARDINFO")
@XmlAccessorType(XmlAccessType.FIELD)
public class RewardProgramPointsInfo {

    /**
     * Name of the reward program, A-32 / MANDATORY
     */
    @XStreamAlias("NAME")
    @XmlElement(name="NAME")
    private String name;

    /**
     *  Current rewards balance as of the time of the download, amount
     *  / MANDATORY
     */
    @XStreamAlias("REWARDBAL")
    @XmlElement(name="REWARDBAL")
    private BigDecimal rewardsBalance;

    /**
     *  Reward amount earned YTD as of the time of download, amount
     */
    @XStreamAlias("REWARDEARNED")
    @XmlElement(name="REWARDEARNED")
    private BigDecimal earnedRewardAmount;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("name", name);
        sbuilder.append("rewardsBalance", rewardsBalance);
        sbuilder.append("earnedRewardAmount", earnedRewardAmount);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getRewardsBalance() {
        return rewardsBalance;
    }

    public void setRewardsBalance(BigDecimal rewardsBalance) {
        this.rewardsBalance = rewardsBalance;
    }

    public BigDecimal getEarnedRewardAmount() {
        return earnedRewardAmount;
    }

    public void setEarnedRewardAmount(BigDecimal earnedRewardAmount) {
        this.earnedRewardAmount = earnedRewardAmount;
    }
}
