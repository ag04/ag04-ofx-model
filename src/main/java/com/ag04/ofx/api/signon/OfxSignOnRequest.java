package com.ag04.ofx.api.signon;

import com.ag04.ofx.api.OfxConstants;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.ag04.ofx.api.OfxFinancialInstitution;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The signon record identifies and authenticates a user to an FI. It also includes information about the application making the request, because some services might be appropriate only for certain clients.
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("SONRQ")
@XmlRootElement(name="SONRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfxSignOnRequest {

	/**
	 * DTCLIENT -Date and time of the request from the client computer, datetime.
	 * This value should reflect the time (according to the client machine) when the request file is sent to the server, not the (original) creation time of the request file. 
	 * While not required for existing software, OFX 2.1.1 clients must comply with this rule. 
	 * This clarification is particularly important in error recovery situations in which the request file may be sent to the server after its initial creation
	 */
	@XStreamAlias("DTCLIENT")
	@XmlElement(name="DTCLIENT")
	private DateTime requestDateTime;
	
	/**
	 * USERID - User identification string. 
	 * To ensure security and help prevent identity fraud, Financial Institutions are discouraged from using Social Security Number for Customer ID or PIN/Password. A-32
	 */
	@XStreamAlias("USERID")
	@XmlElement(name="USERID")
	private String userId;
	
	/**
	 * USERPASS - User password on server, A-171.
	 */
	@XStreamAlias("USERPASS")
	@XmlElement(name="USERPASS")
	private String password;
	
	/**
	 * USERKEY - Log in using previously authenticated context, A-64
	 */
	@XStreamAlias("USERKEY")
	@XmlElement(name="USERKEY")
	private String userkey;

	/**
	 * GENUSERKEY - Request server to return a USERKEY for future use, Boolean
	 */
	@XStreamAlias("GENUSERKEY")
	@XmlElement(name="GENUSERKEY")
	private Boolean generateUserKey;

	/**
	 * LANGUAGE - Requested language for text responses, language.
	 * 
	 * Language is specified as a three-letter code based on ISO-639.
	 */
	@XStreamAlias("LANGUAGE")
	@XmlElement(name="LANGUAGE")
	private String language;
	
	@XStreamAlias("FI")
	@XmlElement(name="FI")
	private OfxFinancialInstitution financialInstitution;
	
	/**
	 * SESSCOOKIE - Session cookie value received in previous <SONRS>, not sent if first login or if none sent by FI, A-1000.
	 */
	@XStreamAlias("SESSCOOKIE")
	@XmlElement(name="SESSCOOKIE")
	private String cookieValue;
	
	/**
	 * APPID - ID of client application, A-5ID of client application, A-5.
	 */
	@XStreamAlias("APPID")
	@XmlElement(name="APPID")
	private String clienAppId;
	
	/**
	 * APPVER - Version of client application, (6.00 encoded as 0600), N-4
	 */
	@XStreamAlias("APPVER")
	@XmlElement(name="APPVER")
	private String clientAppVersion;
	
	/**
	 * CLIENTUID - Unique ID identifying OFX client, A-36
	 */
	@XStreamAlias("CLIENTUID")
	@XmlElement(name="CLIENTUID")
	private String clientId;
	
	/**
	 * USERCRED1 - Additional user credential required by server, A-171
	 * 
	 * Note: the effective size of USERCRED1 is A-32. However, if Type 1 security is used, then the actual field length is A-171.
	 */
	@XStreamAlias("USERCRED1")
	@XmlElement(name="USERCRED1")
	private String userCredentialsOne;
	
	/**
	 * USERCRED2 - Additional user credential required by server, A-171
	 * 
	 * Note: the effective size of USERCRED2 is A-32. However, if Type 1 security is used, then the actual field length is A-171.
	 */
	@XStreamAlias("USERCRED2")
	@XmlElement(name="USERCRED2")
	private String userCredentialsTwo;
	
	/**
	 * AUTHTOKEN - Authentication token required for this signon session only. Credential is provided to the user out of band, A-171.
	 * 
	 * Note: the effective size of AUTHTOKEN is A-32. However, if Type 1 security is used, then the actual field length is A-171.
	 */
	@XStreamAlias("AUTHTOKEN")
	@XmlElement(name="AUTHTOKEN")
	private String authenticationToken;
	
	/**
	 * ACCESSKEY - Access key value received in previous <SONRS>, not sent if first login or none sent by FI, A-1000
	 */
	@XStreamAlias("ACCESSKEY")
	@XmlElement(name="ACCESSKEY")
	private String accessKey;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("requestDateTime", requestDateTime);
		sbuilder.append("userId", userId);
		sbuilder.append("password", password);
		sbuilder.append("userkey", userkey);
		sbuilder.append("generateUserKey", generateUserKey);
		sbuilder.append("language", language);
		sbuilder.append("financialInstitution", financialInstitution);
		sbuilder.append("cookieValue", cookieValue);
		sbuilder.append("clienAppId", clienAppId);
		sbuilder.append("clientAppVersion", clientAppVersion);
		sbuilder.append("clientId", clientId);
		sbuilder.append("userCredentialsOne", userCredentialsOne);
		sbuilder.append("userCredentialsTwo", userCredentialsTwo);
		sbuilder.append("authenticationToken", authenticationToken);
		sbuilder.append("accessKey", accessKey);
		return sbuilder.toString();
	}

	public boolean isAnonymousSignOnRequest() {
		if (OfxConstants.ANONYMOUS_SIGNON_CREDEMTIALS.equalsIgnoreCase(userId)) {
			return true;
		}
		return false;
	}
	
	//--- set / get methods ---------------------------------------------------

	public DateTime getRequestDateTime() {
		return requestDateTime;
	}

	public void setRequestDateTime(DateTime requestDateTime) {
		this.requestDateTime = requestDateTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserkey() {
		return userkey;
	}

	public void setUserkey(String userkey) {
		this.userkey = userkey;
	}

	public Boolean getGenerateUserKey() {
		return generateUserKey;
	}

	public void setGenerateUserKey(Boolean generateUserKey) {
		this.generateUserKey = generateUserKey;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public OfxFinancialInstitution getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(OfxFinancialInstitution financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	public String getCookieValue() {
		return cookieValue;
	}

	public void setCookieValue(String cookieValue) {
		this.cookieValue = cookieValue;
	}

	public String getClienAppId() {
		return clienAppId;
	}

	public void setClienAppId(String clienAppId) {
		this.clienAppId = clienAppId;
	}

	public String getClientAppVersion() {
		return clientAppVersion;
	}

	public void setClientAppVersion(String clientAppVersion) {
		this.clientAppVersion = clientAppVersion;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserCredentialsOne() {
		return userCredentialsOne;
	}

	public void setUserCredentialsOne(String userCredentialsOne) {
		this.userCredentialsOne = userCredentialsOne;
	}

	public String getUserCredentialsTwo() {
		return userCredentialsTwo;
	}

	public void setUserCredentialsTwo(String userCredentialsTwo) {
		this.userCredentialsTwo = userCredentialsTwo;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
}
