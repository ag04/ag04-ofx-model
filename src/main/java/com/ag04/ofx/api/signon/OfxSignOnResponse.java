package com.ag04.ofx.api.signon;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.ag04.ofx.api.OfxFinancialInstitution;
import com.ag04.ofx.api.OfxStatus;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("SONRS")
@XmlRootElement(name="SONRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfxSignOnResponse {
	
	@XStreamAlias("STATUS")
	@XmlElement(name="STATUS")
	private OfxStatus status;
	/**
	 * Date and time of the server response, datetime.
	 * 
	 * This value should reflect the time (according to the server) when the response file was originally created. 
	 * While not required for existing software, OFX 2.1.1 servers must comply with this rule. 
	 * This clarification is particularly important in error recovery situations: The server should (must for OFX 2.1.1 servers) return the time the request was first processed. 
	 * If the previous attempt failed after transactions were processed, <DTSERVER> in the response file would reflect that processing time.
	 */
	@XStreamAlias("DTSERVER")
	@XmlElement(name="DTSERVER")
	private DateTime serverDateTime;
	
	/**
	 * Use user key instead of USERID and USERPASS for subsequent requests. TSKEYEXPIRE can limit lifetime. A-64
	 */
	@XStreamAlias("USERKEY")
	@XmlElement(name="USERKEY")
	private String userkey;
	
	/**
	 * TSKEYEXPIRE - Date and time that USERKEY expires, datetime
	 */
	@XStreamAlias("TSKEYEXPIRE")
	@XmlElement(name="TSKEYEXPIRE")
	private DateTime userKeyExpiresDate;
	
	/**
	 * Language used in text responses, language.
	 * 
	 */
	@XStreamAlias("LANGUAGE")
	@XmlElement(name="LANGUAGE")
	private String language;
	
	/**
	 * DTPROFUP - Date and time of last update to profile information for any service supported by this FI (see Chapter 7, "FI Profile"), datetime
	 */
	@XStreamAlias("DTPROFUP")
	@XmlElement(name="DTPROFUP")
	private DateTime profileLastUpdateDate;
	
	/**
	 * Date and time of last update to account information (see Chapter 8, “Activation & Account Information”), datetime
	 */
	@XStreamAlias("DTACCTUP")
	@XmlElement(name="DTACCTUP")
	private DateTime accountLastUpdateDate;
	
	@XStreamAlias("FI")
	@XmlElement(name="FI")
	private OfxFinancialInstitution financialInstitution;
	
	/**
	 * Session cookie that the client should return on the next <SONRQ>,A-1000
	 */
	@XStreamAlias("SESSCOOKIE")
	@XmlElement(name="SESSCOOKIE")
	private String cookieValue;
	
	/**
	 * Access key that the client should send in the next <SONRQ>, A-1000
	 */
	@XStreamAlias("ACCESSKEY")
	@XmlElement(name="ACCESSKEY")
	private String accessKey;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("status", status);
		sbuilder.append("serverDateTime", serverDateTime);
		sbuilder.append("userkey", userkey);
		sbuilder.append("userKeyExpiresDate", userKeyExpiresDate);
		sbuilder.append("accountLastUpdateDate", accountLastUpdateDate);
		sbuilder.append("profileLastUpdateDate", profileLastUpdateDate);
		sbuilder.append("language", language);
		sbuilder.append("financialInstitution", financialInstitution);
		sbuilder.append("cookieValue", cookieValue);
		sbuilder.append("accessKey", accessKey);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	
	public DateTime getServerDateTime() {
		return serverDateTime;
	}

	public void setServerDateTime(DateTime serverDateTime) {
		this.serverDateTime = serverDateTime;
	}

	public String getUserkey() {
		return userkey;
	}

	public void setUserkey(String userkey) {
		this.userkey = userkey;
	}

	public DateTime getUserKeyExpiresDate() {
		return userKeyExpiresDate;
	}

	public void setUserKeyExpiresDate(DateTime userKeyExpiresDate) {
		this.userKeyExpiresDate = userKeyExpiresDate;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public DateTime getProfileLastUpdateDate() {
		return profileLastUpdateDate;
	}

	public void setProfileLastUpdateDate(DateTime profileLastUpdateDate) {
		this.profileLastUpdateDate = profileLastUpdateDate;
	}

	public OfxFinancialInstitution getFinancialInstitution() {
		return financialInstitution;
	}

	public void setFinancialInstitution(OfxFinancialInstitution financialInstitution) {
		this.financialInstitution = financialInstitution;
	}

	public String getCookieValue() {
		return cookieValue;
	}

	public void setCookieValue(String cookieValue) {
		this.cookieValue = cookieValue;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public OfxStatus getStatus() {
		return status;
	}

	public void setStatus(OfxStatus status) {
		this.status = status;
	}

	public DateTime getAccountLastUpdateDate() {
		return accountLastUpdateDate;
	}

	public void setAccountLastUpdateDate(DateTime accountLastUpdateDate) {
		this.accountLastUpdateDate = accountLastUpdateDate;
	}
}
