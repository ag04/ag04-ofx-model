package com.ag04.ofx.api.signon;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author domagoj
 *
 */
@XStreamAlias("SIGNONMSGSRSV1")
@XmlRootElement(name="SIGNONMSGSRSV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class SignOnMessageSetResponse {

	@XStreamAlias("SONRS")
	@XmlElement(name="SONRS")
	private OfxSignOnResponse signOnResponse;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("signOnResponse", signOnResponse);
		return sbuilder.toString();
	}
	
	//--- set  / get methods --------------------------------------------------

	public OfxSignOnResponse getSignOnResponse() {
		return signOnResponse;
	}

	public void setSignOnResponse(OfxSignOnResponse signOnResponse) {
		this.signOnResponse = signOnResponse;
	}
}
