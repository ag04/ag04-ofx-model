package com.ag04.ofx.api.signon;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Signon message set includes:
 * <ul>
 * <li> the signon message,</li>
 * <li> USERPASS change message,</li>
 * <li> challenge message, and </li>
 * <li> multi-factor authentication (MFA) challenge message,</li>
 * </ul>
 *  which must appear in that order. The "SIGNONMSGSRQV1" and "SIGNONMSGSRSV1" aggregates wrap the message.
 *  
 * @author dmadunic
 *
 */
@XStreamAlias("SIGNONMSGSRQV1")
@XmlRootElement(name="SIGNONMSGSRQV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class SignOnMessageSetRequest {

	/**
	 * 
	 */
	@XStreamAlias("SONRQ")
	@XmlElement(name="SONRQ")
	private OfxSignOnRequest signOnRequest;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("signOnRequest", signOnRequest);
		return sbuilder.toString();
	}

	//--- set / get methods ---------------------------------------------------
	
	public OfxSignOnRequest getSignOnRequest() {
		return signOnRequest;
	}

	public void setSignOnRequest(OfxSignOnRequest signOnRequest) {
		this.signOnRequest = signOnRequest;
	}
	
}
