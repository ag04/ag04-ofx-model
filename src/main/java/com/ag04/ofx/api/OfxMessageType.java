package com.ag04.ofx.api;

/**
 * Enum describing Ofx Message types.
 *
 * Created by dmadunic on 28/05/16.
 */
public enum OfxMessageType {
    BASIC_REQUEST, MODIFY_REQUEST, DELETE_REQUEST, CANCEL_REQUEST, BASIC_RESPONSE, MODIFY_RESPONSE, DELETE_RESPONSE, CANCEL_RESPONSE;
}
