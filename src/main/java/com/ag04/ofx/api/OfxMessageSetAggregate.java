package com.ag04.ofx.api;

/**
 *
 * Created by domagoj on 29/05/16.
 */
public interface OfxMessageSetAggregate {

    boolean supports(TransactionWrapper transactionWrapper);

    void set(TransactionWrapper transactionWrapper);
}
