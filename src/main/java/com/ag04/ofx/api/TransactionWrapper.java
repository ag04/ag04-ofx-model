package com.ag04.ofx.api;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * With the exception of the <SONRQ>/<SONRS> message, each message has a corresponding transaction wrapper. For requests, the transaction wrapper adds a transaction unique ID <TRNUID>. For responses, the transaction wrapper adds the same transaction unique ID <TRNUID> (an echo of that found in the request), plus a <STATUS> aggregate.
 * The transaction wrapper has a name structure of <xxxTRNRQ>/<xxxTRNRS>. A transaction wrapper pair encapsulates a single message (<xxxRQ>/<xxxRS>, <xxxMODRQ>/<xxxMODRS>, etc.).
 * OFX 2.1.1 Specification 5/1/06 43
 * While the same name may be used for addition, modification and deletion messages, a single transaction wrapper may contain at most one request or response. The request transaction wrapper must contain a single request. The response transaction wrapper must contain a single response unless the contained <STATUS> aggregate indicates an error.
 * 
 * @author dmadunic
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class TransactionWrapper {
	/**
	 * TRNUID - transaction unique ID.
	 * 
	 */
	@XStreamAlias("TRNUID")
	@XmlElement(name="TRNUID")
	private String transactionId;
	
	/**
	 * CLTCOOKIE - Data to be echoed in the transaction response, A-32.
	 */
	@XStreamAlias("CLTCOOKIE")
	@XmlElement(name="CLTCOOKIE")
	private String ctrlCookie;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("transactionId", transactionId);
		sbuilder.append("ctrlCookie", ctrlCookie);
		return sbuilder.toString();
	}

	public abstract String ofxElementName();
	
	//--- set / get methods ---------------------------------------------------

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getCtrlCookie() {
		return ctrlCookie;
	}

	public void setCtrlCookie(String ctrlCookie) {
		this.ctrlCookie = ctrlCookie;
	}
}
