package com.ag04.ofx.api.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("ORIGCURRENCY")
@XmlRootElement(name="ORIGCURRENCY")
@XmlAccessorType(XmlAccessType.FIELD)
public class OriginalCurrency extends CurrencyAggregate {

}
