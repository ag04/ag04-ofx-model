package com.ag04.ofx.api.common;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("BAL")
@XmlRootElement(name="BAL")
@XmlAccessorType(XmlAccessType.FIELD)
public class BalanceAggregate {
	/**
	 * Balance name, A-32
	 */
	@XStreamAlias("NAME")
	@XmlElement(name="NAME")
	private String name;
	
	/**
	 * Balance description, A-80
	 */
	@XStreamAlias("DESC")
	@XmlElement(name="DESC")
	private String description;
		
	/**
	 * Balance type.
	 * DOLLAR = dollar (value formatted DDDD.cc)
	 * PERCENT = percentage (value formatted XXXX.YYYY)
	 * NUMBER = number (value formatted as is)
	 */
	@XStreamAlias("BALTYPE")
	@XmlElement(name="BALTYPE")
	private BalanceType type;
	
	/**
	 * Balance value.
	 * Interpretation depends on <BALTYPE> field, amount
	 */
	@XStreamAlias("VALUE")
	@XmlElement(name="VALUE")
	private BigDecimal amount;
	
	/**
	 * Effective date of the given balance, datetime 
	 */
	@XStreamAlias("DTASOF")
	@XmlElement(name="DTASOF")
	private DateTime date;
	
	/**
	 * If dollar formatting, can optionally include currency, see section 5.2 
	 */
	@XStreamAlias("CURRENCY")
	@XmlElement(name="CURRENCY")
	private Currency currency;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("name", name);
		sbuilder.append("description", description);
		sbuilder.append("type", type);
		sbuilder.append("amount", amount);
		sbuilder.append("date", date);
		sbuilder.append("currency", currency);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BalanceType getType() {
		return type;
	}

	public void setType(BalanceType type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	
}
