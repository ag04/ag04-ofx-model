package com.ag04.ofx.api.common;
/**
 * Balance type.
 * DOLLAR = dollar (value formatted DDDD.cc)
 * PERCENT = percentage (value formatted XXXX.YYYY)
 * NUMBER = number (value formatted as is)
 * 
 * @author dmadunic
 *
 */
public enum BalanceType {
	DOLLAR, PERCENT, NUMBER;
}
