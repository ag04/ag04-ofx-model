package com.ag04.ofx.api.common;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author dmadunic
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class CurrencyAggregate {

	/**
	 * Ratio of <CURDEF> currency to <CURSYM> currency, in decimal notation, rate
	 */
	@XStreamAlias("CURRATE")
	@XmlElement(name="CURRATE")
	private BigDecimal currencyRatio;
	
	/**
	 * ISO-4217 3-letter currency identifier, currsymbol
	 */
	@XStreamAlias("CURSYM")
	@XmlElement(name="CURSYM")
	private String code;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("currencyRatio", currencyRatio);
		sbuilder.append("code", code);
		return sbuilder.toString();
	}

	//--- set / get methods ---------------------------------------------------

	public BigDecimal getCurrencyRatio() {
		return currencyRatio;
	}

	public void setCurrencyRatio(BigDecimal currencyRatio) {
		this.currencyRatio = currencyRatio;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}
