package com.ag04.ofx.api;

import com.ag04.ofx.api.bank.BankMessageSetResponse;
import com.ag04.ofx.api.signup.SignupMessageSetResponse;

/**
 * Created by dmadunic on 29/05/16.
 */
public class OfxResponseUtil {

    public static void addTrxResponseToMessageSet(OfxResponse ofxResponse, TransactionWrapperResponse element) {
        Class elementClazz = element.getClass();
        if (SignupMessageSetResponse.belongsTo(elementClazz)) {
            if (ofxResponse.getSignupMessageSetResponse() == null) {
                ofxResponse.setSignupMessageSetResponse(new SignupMessageSetResponse());
            }
            ofxResponse.getSignupMessageSetResponse().setTrxResponse(element);
        } else if (BankMessageSetResponse.belongsTo(elementClazz)) {
            if (ofxResponse.getBankMessageSetResponse() == null) {
                ofxResponse.setBankMessageSetResponse(new BankMessageSetResponse());
            }
            ofxResponse.getBankMessageSetResponse().setTrxResponse(element);
        } else {
            throw new UnsupportedTransactioWrapper(element);
        }

    }
}
