package com.ag04.ofx.api;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Financial-Institution-identification aggregate.
 * 
 * Some service providers support multiple FIs, and assign each FI an ID. 
 * The signon allows clients to pass this information along, so that providers know to which FI the user is signing on.
 * (p. 53)
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("FI")
@XmlRootElement(name="FI")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfxFinancialInstitution {

	/**
	 * ORG -Organization defining this FI name space, A-32
	 * 
	 */
	@XStreamAlias("ORG")
	@XmlElement(name="ORG")
	private String organization;
	
	/**
	 * FID - Financial Institution ID (unique within <ORG>).
	 */
	@XStreamAlias("FID")
	@XmlElement(name="FID")
	private String id;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("organization", organization);
		sbuilder.append("id", id);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
