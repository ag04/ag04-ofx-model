package com.ag04.ofx.api;
/**
 * Place holder for various constants.
 * 
 * @author dmadunic
 *
 */
public class OfxConstants {

	// XML Date formats ...
	public static final String XML_DATE_SHORT_FORMAT = "yyyyMMdd";
	public static final String XML_DATE_MEDIUM_FORMAT = XML_DATE_SHORT_FORMAT + "HHmmss";
	public static final String XML_DATE_LONG_FORMAT = XML_DATE_MEDIUM_FORMAT + ".SSS";
	public static final String XML_DATE_FULL_FORMAT = XML_DATE_LONG_FORMAT + "[ZZ]";
	
	public static final int XML_DATE_FULL_FORMAT_LENGHT = 25;
	public static final int XML_DATE_FULL_FORMAT_WITH_SHORT_TZ_LENGHT = 23;

	public static final String ANONYMOUS_SIGNON_CREDEMTIALS = "anonymous00000000000000000000000";
	
}
