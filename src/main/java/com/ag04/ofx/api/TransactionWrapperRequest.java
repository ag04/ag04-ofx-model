package com.ag04.ofx.api;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * With the exception of the <SONRQ>/<SONRS> message, each message has a corresponding transaction wrapper. For requests, the transaction wrapper adds a transaction unique ID <TRNUID>. For responses, the transaction wrapper adds the same transaction unique ID <TRNUID> (an echo of that found in the request), plus a <STATUS> aggregate.
 * The transaction wrapper has a name structure of <xxxTRNRQ>/<xxxTRNRS>. A transaction wrapper pair encapsulates a single message (<xxxRQ>/<xxxRS>, <xxxMODRQ>/<xxxMODRS>, etc.).
 * OFX 2.1.1 Specification 5/1/06 43
 * While the same name may be used for addition, modification and deletion messages, a single transaction wrapper may contain at most one request or response. The request transaction wrapper must contain a single request. The response transaction wrapper must contain a single response unless the contained <STATUS> aggregate indicates an error.
 * 
 * @author dmadunic
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class TransactionWrapperRequest extends TransactionWrapper {
	/**
	 * TAN - Transaction authorization number; used in some countries with some types of transactions. 
	 * The FI Profile defines messages that require a <TAN>, A-80.
	 * 
	 */
	@XStreamAlias("TAN")
	@XmlElement(name="TAN")
	private String authorizationNumber;

	public TransactionWrapperResponse buildResponse() {
		return buildResponse(null);
	}

	public abstract TransactionWrapperResponse buildResponse(OfxStatus ofxStatus);

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("authorizationNumber", authorizationNumber);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	public String getAuthorizationNumber() {
		return authorizationNumber;
	}

	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}
}
