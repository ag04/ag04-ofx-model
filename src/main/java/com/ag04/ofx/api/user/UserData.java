package com.ag04.ofx.api.user;

import com.ag04.ofx.api.extras.user.Gender;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.LocalDate;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author dmadunic
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class UserData {
	/**
	 * Electronic e-mail address, A-80 / M
	 */
	@XStreamAlias("EMAIL")
	@XmlElement(name="EMAIL")
    private String            mail;
	
	/**
	 * Actual user ID if already known, or preferred user ID if user can choose, A-32
	 */
	@XStreamAlias("USERID")
	@XmlElement(name="USERID")
    private String            userId;
	
	/**
	 * First name of user, A-32 / M
	 */
	@XStreamAlias("FIRSTNAME")
	@XmlElement(name="FIRSTNAME")
    private String            firstName;
	
	/**
	 * Last name of user, A-32 / M
	 */
	@XStreamAlias("LASTNAME")
	@XmlElement(name="LASTNAME")
    private String            lastName;
	
	/**
	 * Middle name of user, A-32
	 */
	@XStreamAlias("MIDDLENAME")
	@XmlElement(name="MIDDLENAME")
    private String            middleName;
	
	/**
	 * Address line 1, A-32 / M
	 */
	@XStreamAlias("ADDR1")
	@XmlElement(name="ADDR1")
    private String            addressOne;
	
	/**
	 * Address line 2, A-32
	 */
	@XStreamAlias("ADDR2")
	@XmlElement(name="ADDR2")
    private String            addressTwo;
	
	/**
	 * Address line 3. Use of <ADDR3> requires the presence of <ADDR2>, A-32
	 */
	@XStreamAlias("ADDR3")
	@XmlElement(name="ADDR3")
    private String            addressThree;
	
	/**
	 * City, A-32
	 */
	@XStreamAlias("CITY")
	@XmlElement(name="CITY")
    private String            city;
	
	/**
	 * State or province, A-5
	 */
	@XStreamAlias("STATE")
	@XmlElement(name="STATE")
    private String            state;
	
	/**
	 * Postal code, A-11 / M
	 */
	@XStreamAlias("POSTALCODE")
	@XmlElement(name="POSTALCODE")
    private String            postalCode;
	
	@XStreamAlias("DAYPHONE")
	@XmlElement(name="DAYPHONE")
    private String            dayPhone;
	
	@XStreamAlias("EVEPHONE")
	@XmlElement(name="EVEPHONE")
    private String            eveningPhone;
	
	@XStreamAlias("TAXID")
	@XmlElement(name="TAXID")
    private String            taxId;
	
	@XStreamAlias("SECURITYNAME")
	@XmlElement(name="SECURITYNAME")
    private String            securityName;
	
	@XStreamAlias("DATEBIRTH")
	@XmlElement(name="DATEBIRTH")
	private LocalDate            dateOfBirth;
	
	/**
	 * 3-letter country code from ISO/DIS-3166, A-3 / M
	 */
	@XStreamAlias("COUNTRY")
	@XmlElement(name="COUNTRY")
	private String countryCode;

	@XStreamAlias("AG04.GENDER")
	@XmlElement(name="AG04.GENDER")
	private Gender gender;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("mail", mail);
		sbuilder.append("userId", userId);
		sbuilder.append("firstName", firstName);
		sbuilder.append("lastName", lastName);
		sbuilder.append("gender", gender);
		sbuilder.append("dateOfBirth", dateOfBirth);
		sbuilder.append("securityName", securityName);
		sbuilder.append("taxId", taxId);
		sbuilder.append("addressOne", addressOne);
		sbuilder.append("addressTwo", addressTwo);
		sbuilder.append("addressThree", addressThree);
		sbuilder.append("countryCode", countryCode);
		sbuilder.append("city", city);
		sbuilder.append("state", state);
		sbuilder.append("postalCode", postalCode);
		sbuilder.append("dayPhone", dayPhone);
		sbuilder.append("eveningPhone", eveningPhone);
		return sbuilder.toString();
	}
	
	//--- set  / get methods --------------------------------------------------

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAddressOne() {
		return addressOne;
	}

	public void setAddressOne(String addressOne) {
		this.addressOne = addressOne;
	}

	public String getAddressTwo() {
		return addressTwo;
	}

	public void setAddressTwo(String addressTwo) {
		this.addressTwo = addressTwo;
	}

	public String getAddressThree() {
		return addressThree;
	}

	public void setAddressThree(String addressThree) {
		this.addressThree = addressThree;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getDayPhone() {
		return dayPhone;
	}

	public void setDayPhone(String dayPhone) {
		this.dayPhone = dayPhone;
	}

	public String getEveningPhone() {
		return eveningPhone;
	}

	public void setEveningPhone(String eveningPhone) {
		this.eveningPhone = eveningPhone;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
}
