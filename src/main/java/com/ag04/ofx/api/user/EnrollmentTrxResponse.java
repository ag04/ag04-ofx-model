package com.ag04.ofx.api.user;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Created by dmadunic on 27/05/16.
 */
@XStreamAlias("ENROLLTRNRS")
@XmlRootElement(name="ENROLLTRNRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnrollmentTrxResponse extends TransactionWrapperResponse {

    @XStreamAlias("ENROLLRS")
    @XmlElement(name="ENROLLRS")
    private EnrollmentResponse enrollmentResponse;

    public EnrollmentTrxResponse() {
        //
    }

    public EnrollmentTrxResponse(EnrollmentTrxRequest request) {
        super(request);
    }

    public EnrollmentTrxResponse(OfxStatus status, EnrollmentTrxRequest request) {
        super(status, request);
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("enrollmentResponse", enrollmentResponse);
        return sbuilder.toString();
    }

    @Override
    public String ofxElementName() {
        return "<ENROLLTRNRS>";
    }

    //--- set / get methods ---------------------------------------------------

    public EnrollmentResponse getEnrollmentResponse() {
        return enrollmentResponse;
    }

    public void setEnrollmentResponse(EnrollmentResponse enrollmentResponse) {
        this.enrollmentResponse = enrollmentResponse;
    }
}
