package com.ag04.ofx.api.user;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("ENROLLRS")
@XmlRootElement(name="ENROLLRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnrollmentResponse {
	
	/**
	 * Temporary password, A-32 
	 */
	@XStreamAlias("TEMPPASS")
	@XmlElement(name="TEMPPASS")
	private String temporaryPassword;
	
	/**
	 * User ID, A-32
	 */
	@XStreamAlias("USERID")
	@XmlElement(name="USERID")
	private String userId;
	
	/**
	 * Time the temporary password expires (if <TEMPPASS> included), datetime
	 */
	@XStreamAlias("DTEXPIRE")
	@XmlElement(name="DTEXPIRE")
	private DateTime expieryDate;
	
	
    @Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("userId", userId);
		sbuilder.append("temporaryPassword", temporaryPassword);
		sbuilder.append("expieryDate", expieryDate);
		return sbuilder.toString();
	}
    
	//--- set  /get methods ---------------------------------------------------

	public String getTemporaryPassword() {
		return temporaryPassword;
	}

	public void setTemporaryPassword(String temporaryPassword) {
		this.temporaryPassword = temporaryPassword;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public DateTime getExpieryDate() {
		return expieryDate;
	}

	public void setExpieryDate(DateTime expieryDate) {
		this.expieryDate = expieryDate;
	}
}
