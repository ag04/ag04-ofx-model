package com.ag04.ofx.api.user;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperRequest;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.ag04.ofx.api.signup.AccountInfoTrxResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Created by dmadunic on 27/05/16.
 */
@XStreamAlias("ENROLLTRNRQ")
@XmlRootElement(name="ENROLLTRNRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnrollmentTrxRequest extends TransactionWrapperRequest {

    @XStreamAlias("ENROLLRQ")
    @XmlElement(name="ENROLLRQ")
    private EnrollmentRequest enrollmentRequest;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("enrollmentRequest", enrollmentRequest);
        return sbuilder.toString();
    }

    @Override
    public TransactionWrapperResponse buildResponse(OfxStatus ofxStatus) {
        if (ofxStatus == null) {
            return new EnrollmentTrxResponse(this);
        }
        return new EnrollmentTrxResponse(ofxStatus, this);
    }

    @Override
    public String ofxElementName() {
        return "<ENROLLTRNRQ>";
    }

    //--- set / get methods ---------------------------------------------------


    public EnrollmentRequest getEnrollmentRequest() {
        return enrollmentRequest;
    }

    public void setEnrollmentRequest(EnrollmentRequest enrollmentRequest) {
        this.enrollmentRequest = enrollmentRequest;
    }
}
