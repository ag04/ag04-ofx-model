package com.ag04.ofx.api.user;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("ENROLLRQ")
@XmlRootElement(name="ENROLLRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnrollmentRequest extends UserData {
	
}
