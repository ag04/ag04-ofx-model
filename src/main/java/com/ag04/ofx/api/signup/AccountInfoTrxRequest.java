package com.ag04.ofx.api.signup;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.TransactionWrapperRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("ACCTINFOTRNRQ")
@XmlRootElement(name="ACCTINFOTRNRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountInfoTrxRequest extends TransactionWrapperRequest {

	@XStreamAlias("ACCTINFORQ")
	@XmlElement(name="ACCTINFORQ")
	private AccountInfoRequest accountInfoRequest;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("accountInfoRequest", accountInfoRequest);
		return sbuilder.toString();
	}

	@Override
	public TransactionWrapperResponse buildResponse(OfxStatus ofxStatus) {
		if (ofxStatus == null) {
			return new AccountInfoTrxResponse(this);
		}
		return new AccountInfoTrxResponse(ofxStatus, this);
	}

	@Override
	public String ofxElementName() {
		return "<ACCTINFOTRNRQ>";
	}

	//--- set / get methods ---------------------------------------------------
	
	public AccountInfoRequest getAccountInfoRequest() {
		return accountInfoRequest;
	}

	public void setAccountInfoRequest(AccountInfoRequest request) {
		this.accountInfoRequest = request;
	}
}
