package com.ag04.ofx.api.signup;

import java.util.ArrayList;
import java.util.List;

import com.ag04.ofx.api.account.AccountInfo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import javax.xml.bind.annotation.*;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("ACCTINFORS")
@XmlRootElement(name="ACCTINFORS")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountInfoResponse {

	/**
	 * DTACCTUP - Last "DTACCTUP" received in a response, datetime
	 */
	@XStreamAlias("DTACCTUP")
	@XmlElement(name="DTACCTUP")
	private DateTime lastAccountUpdateDate;
	
	@XStreamImplicit(itemFieldName="ACCTINFO")
	@XmlElement(name="ACCTINFO")
	private List<AccountInfo> accounts;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("lastAccountUpdateDate", lastAccountUpdateDate);
		sbuilder.append("accounts", accounts);
		return sbuilder.toString();
	}

	public void addAccountInfo(AccountInfo accountInfo) {
		if (accounts == null) {
			accounts = new ArrayList<>();
		}
		accounts.add(accountInfo);
	}

	//--- set / get methods ---------------------------------------------------
	
	public DateTime getLastAccountUpdateDate() {
		return lastAccountUpdateDate;
	}

	public void setLastAccountUpdateDate(DateTime lastAccountUpdateDate) {
		this.lastAccountUpdateDate = lastAccountUpdateDate;
	}

	public List<AccountInfo> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountInfo> accounts) {
		this.accounts = accounts;
	}
	
}
