package com.ag04.ofx.api.signup;

import com.ag04.ofx.api.OfxRequestMessageSetAggregate;
import com.ag04.ofx.api.TransactionWrapperRequest;
import com.ag04.ofx.api.user.EnrollmentTrxRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Signup message set defines three messages to help users get setup with their FI:
 *
 * <ul>
 *  <li>Enrollment – informs FI that a user wants to use OFX and requests that a password be returned</li>
 *  <li>Accounts – asks the FI to return a list of accounts and the services supported for each account</li>
 *  <li>Activation – allows a client to tell the FI which services a user wants on each account</li>
 * </ul>
 *
 * There is also a message to request name and address changes.
 *
 * Clients use the account information request on a regular basis to look for changes in a user’s account information.
 * A time stamp is part of the request so that a server has to report only new changes.
 * Account activation requests are subject to data synchronization, and will allow multiple clients to learn how the other clients have been enabled.
 *
 * In OFX request files, the <SIGNUPMSGSRQV1> aggregate identifies the Signup messages.
 *
 * Created by dmadunic on 27/05/16.
 */
@XStreamAlias("SIGNUPMSGSRQV1")
@XmlRootElement(name="SIGNUPMSGSRQV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class SignupMessageSetRequest implements OfxRequestMessageSetAggregate, Serializable {

    @XStreamAlias("ACCTINFOTRNRQ")
    @XmlElement(name="ACCTINFOTRNRQ")
    private AccountInfoTrxRequest accountInfoTrxRequest;

    @XStreamAlias("ENROLLTRNRQ")
    @XmlElement(name="ENROLLTRNRQ")
    private EnrollmentTrxRequest enrollmentTrxRequest;


    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("accountInfoTrxRequest", accountInfoTrxRequest);
        sbuilder.append("enrollmentTrxRequest", enrollmentTrxRequest);
        return sbuilder.toString();
    }

    //-- interface methods ----------------------------------------------------

    /**
     * Returns all (not null) available TransactionRequestWrappers in this message set.
     *
     * @return
     */
    @Override
    public List<TransactionWrapperRequest> getAllTrxRequests() {
        List<TransactionWrapperRequest> wrappers = new ArrayList<TransactionWrapperRequest>();
        if (accountInfoTrxRequest != null) {
            wrappers.add(accountInfoTrxRequest);
        }
        if (enrollmentTrxRequest != null) {
            wrappers.add(enrollmentTrxRequest);
        }
        return wrappers;
    }

    //--- set  / get methods --------------------------------------------------

    public AccountInfoTrxRequest getAccountInfoTrxRequest() {
        return accountInfoTrxRequest;
    }

    public void setAccountInfoTrxRequest(AccountInfoTrxRequest accountInfoTrxRequest) {
        this.accountInfoTrxRequest = accountInfoTrxRequest;
    }

    public EnrollmentTrxRequest getEnrollmentTrxRequest() {
        return enrollmentTrxRequest;
    }

    public void setEnrollmentTrxRequest(EnrollmentTrxRequest enrollmentTrxRequest) {
        this.enrollmentTrxRequest = enrollmentTrxRequest;
    }
}
