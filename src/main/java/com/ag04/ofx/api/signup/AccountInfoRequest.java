package com.ag04.ofx.api.signup;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */

@XStreamAlias("ACCTINFORQ")
@XmlRootElement(name="ACCTINFORQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountInfoRequest {

	/**
	 * DTACCTUP - Last "DTACCTUP" received in a response, datetime
	 */
	@XStreamAlias("DTACCTUP")
	@XmlElement(name="DTACCTUP")
	private DateTime lastAccountUpdateDate;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("lastAccountUpdateDate", lastAccountUpdateDate);
		return sbuilder.toString();
	}

	//--- set / get methods ---------------------------------------------------
	
	public DateTime getLastAccountUpdateDate() {
		return lastAccountUpdateDate;
	}

	public void setLastAccountUpdateDate(DateTime lastAccountUpdateDate) {
		this.lastAccountUpdateDate = lastAccountUpdateDate;
	}
	
}
