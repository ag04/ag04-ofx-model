package com.ag04.ofx.api.signup;

import com.ag04.ofx.api.OfxResponseMessageSetAggregate;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.ag04.ofx.api.UnsupportedTransactioWrapper;
import com.ag04.ofx.api.user.EnrollmentTrxResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.*;

/**
 * Created by dmadunic on 27/05/16.
 */
@XStreamAlias("SIGNUPMSGSRSV1")
@XmlRootElement(name="SIGNUPMSGSRSV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class SignupMessageSetResponse implements OfxResponseMessageSetAggregate, Serializable {

    public static final Class[] SUPPORTED_TRANSACTIONS = new Class[] { AccountInfoTrxResponse.class, EnrollmentTrxResponse.class };

    public static Set<Class> supportedTransactions = new HashSet<Class>(Arrays.asList(SUPPORTED_TRANSACTIONS));

    @XStreamAlias("ACCTINFOTRNRS")
    @XmlElement(name="ACCTINFOTRNRS")
    private AccountInfoTrxResponse accountInfoTrxResponse;

    @XStreamAlias("ENROLLTRNRS")
    @XmlElement(name="ENROLLTRNRS")
    private EnrollmentTrxResponse enrollmentTrxResponse;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append("accountInfoTrxResponse", accountInfoTrxResponse);
        sbuilder.append("enrollmentTrxResponse", enrollmentTrxResponse);
        return sbuilder.toString();
    }

    public static boolean belongsTo(Class clazz) {
        return supportedTransactions.contains(clazz);
    }

    public void setTrxResponse(TransactionWrapperResponse response) {
        if (response instanceof AccountInfoTrxResponse) {
            accountInfoTrxResponse = (AccountInfoTrxResponse) response;
        } else if (response instanceof EnrollmentTrxResponse) {
            enrollmentTrxResponse = (EnrollmentTrxResponse) response;
        } else {
            throw new UnsupportedTransactioWrapper(response);
        }
    }

    //-- interface methods ----------------------------------------------------

    /**
     * Returns all (not null) available TransactionResponseWrappers in this message set.
     *
     * @return
     */
    @Override
    public List<TransactionWrapperResponse> getAllTrxResponses() {
        List<TransactionWrapperResponse> wrappers = new ArrayList<TransactionWrapperResponse>();
        if (accountInfoTrxResponse != null) {
            wrappers.add(accountInfoTrxResponse);
        }
        if (enrollmentTrxResponse != null) {
            wrappers.add(enrollmentTrxResponse);
        }
        return wrappers;
    }

    //--- set / get methods ---------------------------------------------------

    public AccountInfoTrxResponse getAccountInfoTrxResponse() {
        return accountInfoTrxResponse;
    }

    public void setAccountInfoTrxResponse(AccountInfoTrxResponse accountInfoTrxResponse) {
        this.accountInfoTrxResponse = accountInfoTrxResponse;
    }

    public EnrollmentTrxResponse getEnrollmentTrxResponse() {
        return enrollmentTrxResponse;
    }

    public void setEnrollmentTrxResponse(EnrollmentTrxResponse enrollmentTrxResponse) {
        this.enrollmentTrxResponse = enrollmentTrxResponse;
    }
}
