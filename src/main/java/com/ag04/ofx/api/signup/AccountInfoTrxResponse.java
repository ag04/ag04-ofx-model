package com.ag04.ofx.api.signup;

import com.ag04.ofx.api.OfxStatus;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.TransactionWrapperResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("ACCTINFOTRNRS")
@XmlRootElement(name="ACCTINFOTRNRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountInfoTrxResponse extends TransactionWrapperResponse {

	@XStreamAlias("ACCTINFORS")
	@XmlElement(name="ACCTINFORS")
	private AccountInfoResponse response;
	
	public AccountInfoTrxResponse() {
		//
	}
	
	public AccountInfoTrxResponse(AccountInfoTrxRequest request) {
		super(request);
	}

	public AccountInfoTrxResponse(OfxStatus status, AccountInfoTrxRequest request) {
		super(status, request);
	}

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("accountInfoResponse", response);
		return sbuilder.toString();
	}

	@Override
	public String ofxElementName() {
		return "<ACCTINFOTRNRS>";
	}

	//--- set / get methods ---------------------------------------------------
	
	public AccountInfoResponse getResponse() {
		return response;
	}

	public void setResponse(AccountInfoResponse response) {
		this.response = response;
	}
}
