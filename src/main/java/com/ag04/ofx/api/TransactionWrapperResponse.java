package com.ag04.ofx.api;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.springframework.util.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * With the exception of the <SONRQ>/<SONRS> message, each message has a corresponding transaction wrapper. For requests, the transaction wrapper adds a transaction unique ID <TRNUID>. For responses, the transaction wrapper adds the same transaction unique ID <TRNUID> (an echo of that found in the request), plus a <STATUS> aggregate.
 * The transaction wrapper has a name structure of <xxxTRNRQ>/<xxxTRNRS>. A transaction wrapper pair encapsulates a single message (<xxxRQ>/<xxxRS>, <xxxMODRQ>/<xxxMODRS>, etc.).
￼￼￼￼￼ * OFX 2.1.1 Specification 5/1/06 43

 * While the same name may be used for addition, modification and deletion messages, a single transaction wrapper may contain at most one request or response. 
 * The request transaction wrapper must contain a single request. 
 * The response transaction wrapper must contain a single response unless the contained <STATUS> aggregate indicates an error.
 * 
 * 
 * @author dmadunic
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class TransactionWrapperResponse extends TransactionWrapper {

	/**
	 * Status of this Transaction block
	 */
	@XStreamAlias("STATUS")
	@XmlElement(name="STATUS")
	private OfxStatus status;

	public TransactionWrapperResponse() {
		//
	}
	
	public TransactionWrapperResponse(TransactionWrapperRequest request) {
		setTransactionId(request.getTransactionId());
		if (StringUtils.hasText(request.getCtrlCookie())) {
			setCtrlCookie(request.getCtrlCookie());
		}
	}

	public TransactionWrapperResponse(OfxStatus status, TransactionWrapperRequest request) {
		this(request);
		this.status = status;
	}
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("status", status);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------

	public OfxStatus getStatus() {
		return status;
	}

	public void setStatus(OfxStatus status) {
		this.status = status;
	}
	
}
