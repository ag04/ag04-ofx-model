package com.ag04.ofx.api;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("STATUS")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfxStatus {

	@XStreamAlias("CODE")
	@XmlElement(name="CODE")
	private Long code;

	@XStreamAlias("SEVERITY")
	@XmlElement(name="SEVERITY")
	private OfxSeverity severity;

	@XStreamAlias("MESSAGE")
	@XmlElement(name="MESSAGE")
	private String message;
	
	public OfxStatus() {
		//
	}
	
	public OfxStatus(Long code) {
		this(code, null, null);
	}

	public OfxStatus(Long code, OfxSeverity severity) {
		this(code, null, severity);
	}

	public OfxStatus(Long code, String message, OfxSeverity severity) {
		this.code = code;
		this.severity = severity;
		this.message = message;
	}

	public OfxStatus(OfxStatusCode statusCode) {
		this.code = statusCode.getCode();
		this.severity = statusCode.getSeverity();
		this.message = statusCode.getMessage();
	}

	public void set(Long code, String message, OfxSeverity severity) {
		this.code = code;
		this.severity = severity;
		this.message = message;
	}
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("code", code);
		sbuilder.append("severity", severity);
		sbuilder.append("message", message);
		return sbuilder.toString();
	}

	//--- set / get methods ---------------------------------------------------
	
	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}

	public OfxSeverity getSeverity() {
		return severity;
	}

	public void setSeverity(OfxSeverity severity) {
		this.severity = severity;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
