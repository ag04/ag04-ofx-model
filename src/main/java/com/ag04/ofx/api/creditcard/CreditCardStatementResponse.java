package com.ag04.ofx.api.creditcard;

import com.ag04.ofx.api.account.creditcard.CreditCardAccountFromAggregate;
import com.ag04.ofx.api.statement.BaseStatementResponse;
import com.ag04.ofx.api.statement.RewardProgramPointsInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XStreamAlias("CCSTMTRS")
@XmlRootElement(name="CCSTMTRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardStatementResponse extends BaseStatementResponse {

    /**
     * Account from aggregate, see section 11.3.2 / M
     */
    @XStreamAlias("CCACCTFROM")
    @XmlElement(name="CCACCTFROM")
    CreditCardAccountFromAggregate creditCardAccountAggregate;

    /**
     * Available balance amount for cash advances at the time of the download, amount
     */
    @XStreamAlias("CASHADVBALAMT")
    @XmlElement(name="CASHADVBALAMT")
    private BigDecimal cashAdvancesBalanceAmount;

    /**
     * Current interest rate for purchases in effect at time of download
     */
    @XStreamAlias("INTRATEPURCH")
    @XmlElement(name="INTRATEPURCH")
    private BigDecimal purchasesInterestRate;

    /**
     * Current interest rate for balance transfers in effect at time of download
     */
    @XStreamAlias("INTRATEXFER")
    @XmlElement(name="INTRATEXFER")
    private BigDecimal xferInterestRate;

    /**
     * Opening aggregate for reward/points program current information
     *
     * Note:In CCSTMTRS this aggregate is used to return the current values and YTD points earned for the program at the time of the download
     */
    @XStreamAlias("REWARDINFO")
    @XmlElement(name="REWARDINFO")
    private RewardProgramPointsInfo rewardPointsInfo;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("creditCardAccountAggregate", creditCardAccountAggregate);
        sbuilder.append("cashAdvancesBalanceAmount", cashAdvancesBalanceAmount);
        sbuilder.append("purchasesInterestRate", purchasesInterestRate);
        sbuilder.append("xferInterestRate", xferInterestRate);
        sbuilder.append("rewardPointsInfo", rewardPointsInfo);
        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public CreditCardAccountFromAggregate getCreditCardAccountAggregate() {
        return creditCardAccountAggregate;
    }

    public void setCreditCardAccountAggregate(CreditCardAccountFromAggregate creditCardAccountAggregate) {
        this.creditCardAccountAggregate = creditCardAccountAggregate;
    }

    public BigDecimal getCashAdvancesBalanceAmount() {
        return cashAdvancesBalanceAmount;
    }

    public void setCashAdvancesBalanceAmount(BigDecimal cashAdvancesBalanceAmount) {
        this.cashAdvancesBalanceAmount = cashAdvancesBalanceAmount;
    }

    public BigDecimal getPurchasesInterestRate() {
        return purchasesInterestRate;
    }

    public void setPurchasesInterestRate(BigDecimal purchasesInterestRate) {
        this.purchasesInterestRate = purchasesInterestRate;
    }

    public BigDecimal getXferInterestRate() {
        return xferInterestRate;
    }

    public void setXferInterestRate(BigDecimal xferInterestRate) {
        this.xferInterestRate = xferInterestRate;
    }

    public RewardProgramPointsInfo getRewardPointsInfo() {
        return rewardPointsInfo;
    }

    public void setRewardPointsInfo(RewardProgramPointsInfo rewardPointsInfo) {
        this.rewardPointsInfo = rewardPointsInfo;
    }
}
