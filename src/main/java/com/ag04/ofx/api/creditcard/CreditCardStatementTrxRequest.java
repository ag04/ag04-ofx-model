package com.ag04.ofx.api.creditcard;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperRequest;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XStreamAlias("CCSTMTTRNRQ")
@XmlRootElement(name="CCSTMTTRNRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardStatementTrxRequest extends TransactionWrapperRequest {

    @XStreamAlias("CCSTMTRQ")
    @XmlElement(name="CCSTMTRQ")
    private CreditCardStatementRequest creditCardStatementRequest;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("creditCardStatementRequest", creditCardStatementRequest);
        return sbuilder.toString();
    }

    @Override
    public TransactionWrapperResponse buildResponse(OfxStatus ofxStatus) {
        if (ofxStatus == null) {
            return new CreditCardStatementTrxResponse(this);
        }
        return new CreditCardStatementTrxResponse(ofxStatus, this);
    }

    @Override
    public String ofxElementName() {
        return "<CCSTMTTRNRQ>";
    }

    //--- set / get methods ---------------------------------------------------


    public CreditCardStatementRequest getCreditCardStatementRequest() {
        return creditCardStatementRequest;
    }

    public void setCreditCardStatementRequest(CreditCardStatementRequest creditCardStatementRequest) {
        this.creditCardStatementRequest = creditCardStatementRequest;
    }
}
