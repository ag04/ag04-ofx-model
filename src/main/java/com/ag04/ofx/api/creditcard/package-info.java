@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(type = DateTime.class, value = DateTimeAdapter.class),
        @XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateAdapter.class)
})
package com.ag04.ofx.api.creditcard;

import com.ag04.ofx.utils.jaxb.DateTimeAdapter;
import com.ag04.ofx.utils.jaxb.LocalDateAdapter;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

