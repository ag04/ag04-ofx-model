package com.ag04.ofx.api.creditcard;

import com.ag04.ofx.api.OfxResponseMessageSetAggregate;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.ag04.ofx.api.UnsupportedTransactioWrapper;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.*;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XStreamAlias("CREDITCARDMSGSRSV1")
@XmlRootElement(name="CREDITCARDMSGSRSV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardMessageSetResponse implements Serializable, OfxResponseMessageSetAggregate {
    public static final Class[] SUPPORTED_TRANSACTIONS = new Class[] { CreditCardStatementTrxResponse.class };

    public static Set<Class> supportedTransactions = new HashSet<Class>(Arrays.asList(SUPPORTED_TRANSACTIONS));

    @XStreamImplicit(itemFieldName="CCSTMTTRNRS")
    @XmlElement(name="CCSTMTTRNRS")
    private List<CreditCardStatementTrxResponse> creditCardStatementTransactionResponses;

    public static boolean belongsTo(Class clazz) {
        return supportedTransactions.contains(clazz);
    }

    public void setTrxResponse(TransactionWrapperResponse response) {
        if (response instanceof CreditCardStatementTrxResponse) {
            addStatementTransactionResponse((CreditCardStatementTrxResponse) response);
        } else {
            throw new UnsupportedTransactioWrapper(response);
        }
    }

    public void addStatementTransactionResponse(CreditCardStatementTrxResponse creditCardStatementTransactionResponse) {
        if (creditCardStatementTransactionResponses == null) {
            creditCardStatementTransactionResponses = new ArrayList<>();
        }
        creditCardStatementTransactionResponses.add(creditCardStatementTransactionResponse);
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("creditCardStatementTransactionResponses", creditCardStatementTransactionResponses);
        return sbuilder.toString();
    }

    //--- interface methods ---------------------------------------------------

    /**
     * Returns all (not null) available TransactionResponseWrappers in this message set.
     *
     * @return
     */
    @Override
    public List<TransactionWrapperResponse> getAllTrxResponses() {
        List<TransactionWrapperResponse> wrappers = new ArrayList<>();
        if (creditCardStatementTransactionResponses != null) {
            wrappers.addAll(creditCardStatementTransactionResponses);
        }
        return wrappers;
    }

    //--- set / get methods ---------------------------------------------------


    public List<CreditCardStatementTrxResponse> getCreditCardStatementTransactionResponses() {
        return creditCardStatementTransactionResponses;
    }

    public void setCreditCardStatementTransactionResponses(List<CreditCardStatementTrxResponse> creditCardStatementTransactionResponses) {
        this.creditCardStatementTransactionResponses = creditCardStatementTransactionResponses;
    }

}
