package com.ag04.ofx.api.creditcard;

import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XStreamAlias("CCSTMTTRNRS")
@XmlRootElement(name="CCSTMTTRNRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardStatementTrxResponse extends TransactionWrapperResponse {

    @XStreamAlias("CCSTMTRS")
    @XmlElement(name="CCSTMTRS")
    private CreditCardStatementResponse response;

    public CreditCardStatementTrxResponse() {
        // default constructor
    }

    public CreditCardStatementTrxResponse(CreditCardStatementTrxRequest request) {
        super(request);
    }

    public CreditCardStatementTrxResponse(OfxStatus status, CreditCardStatementTrxRequest request) {
        super(status, request);
    }

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("response", response);
        return sbuilder.toString();
    }

    @Override
    public String ofxElementName() {
        return "<CCSTMTTRNRS>";
    }

    //--- set / get methods ---------------------------------------------------


    public CreditCardStatementResponse getResponse() {
        return response;
    }

    public void setResponse(CreditCardStatementResponse response) {
        this.response = response;
    }
}
