package com.ag04.ofx.api.creditcard;

import com.ag04.ofx.api.OfxRequestMessageSetAggregate;
import com.ag04.ofx.api.TransactionWrapperRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.*;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XStreamAlias("CREDITCARDMSGSRQV1")
@XmlRootElement(name="CREDITCARDMSGSRQV1")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardMessageSetRequest implements Serializable, OfxRequestMessageSetAggregate {
    public static final Class[] SUPPORTED_TRANSACTIONS = new Class[] { CreditCardStatementTrxRequest.class };

    public static Set<Class> supportedTransactions = new HashSet<Class>(Arrays.asList(SUPPORTED_TRANSACTIONS));

    @XStreamImplicit(itemFieldName="CCSTMTTRNRQ")
    @XmlElement(name="CCSTMTTRNRQ")
    private List<CreditCardStatementTrxRequest> creditCardStatementTrxRequests;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("creditCardStatementTrxRequests", creditCardStatementTrxRequests);
        return sbuilder.toString();
    }

    /**
     * Returns all (not null) available TransactionRequestWrappers in this message set.
     *
     * @return
     */
    @Override
    public List<TransactionWrapperRequest> getAllTrxRequests() {
        List<TransactionWrapperRequest> wrappers = new ArrayList<TransactionWrapperRequest>();
        if (creditCardStatementTrxRequests != null) {
            wrappers.addAll(creditCardStatementTrxRequests);
        }

        return wrappers;
    }

    public void addCreditCardStatementTrxRequest(CreditCardStatementTrxRequest creditCardStatementTrxRequest) {
        if (creditCardStatementTrxRequests == null) {
            creditCardStatementTrxRequests = new ArrayList<CreditCardStatementTrxRequest>();
        }
        creditCardStatementTrxRequests.add(creditCardStatementTrxRequest);
    }

    //--- set / get methods ---------------------------------------------------

    public List<CreditCardStatementTrxRequest> getCreditCardStatementTrxRequests() {
        return creditCardStatementTrxRequests;
    }

    public void setCreditCardStatementTrxRequests(List<CreditCardStatementTrxRequest> creditCardStatementTrxRequests) {
        this.creditCardStatementTrxRequests = creditCardStatementTrxRequests;
    }

}
