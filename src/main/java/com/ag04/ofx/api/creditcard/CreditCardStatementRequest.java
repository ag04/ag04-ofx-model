package com.ag04.ofx.api.creditcard;

import com.ag04.ofx.api.account.creditcard.CreditCardAccountFromAggregate;
import com.ag04.ofx.api.statement.IncludeTransactionsAggregate;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by dmadunic on 21/08/2016.
 */
@XStreamAlias("CCSTMTRQ")
@XmlRootElement(name="CCSTMTRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditCardStatementRequest {

    @XStreamAlias("CCACCTFROM")
    @XmlElement(name="CCACCTFROM")
    CreditCardAccountFromAggregate accountAggregate;

    @XStreamAlias("INCTRAN")
    @XmlElement(name="INCTRAN")
    private IncludeTransactionsAggregate includeTransactions;

    @XStreamAlias("INCTRANIMG")
    @XmlElement(name="INCTRANIMG")
    private Boolean includeTrxImage;

    @XStreamAlias("INCLUDEPENDING")
    @XmlElement(name="INCLUDEPENDING")
    private Boolean includePendingTransactions = Boolean.FALSE;

    @Override
    public String toString() {
        final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
        sbuilder.append(super.toString());
        sbuilder.append("accountFromAggregate", accountAggregate);
        sbuilder.append("includeTransactions", includeTransactions);
        sbuilder.append("includeTrxImage", includeTrxImage);
        sbuilder.append("includePendingTransactions", includePendingTransactions);

        return sbuilder.toString();
    }

    //--- set / get methods ---------------------------------------------------

    public CreditCardAccountFromAggregate getAccountAggregate() {
        return accountAggregate;
    }

    public void setAccountAggregate(CreditCardAccountFromAggregate accountAggregate) {
        this.accountAggregate = accountAggregate;
    }

    public IncludeTransactionsAggregate getIncludeTransactions() {
        return includeTransactions;
    }

    public void setIncludeTransactions(IncludeTransactionsAggregate includeTransactions) {
        this.includeTransactions = includeTransactions;
    }

    public Boolean getIncludeTrxImage() {
        return includeTrxImage;
    }

    public void setIncludeTrxImage(Boolean includeTrxImage) {
        this.includeTrxImage = includeTrxImage;
    }

    public Boolean getIncludePendingTransactions() {
        return includePendingTransactions;
    }

    public void setIncludePendingTransactions(Boolean includePendingTransactions) {
        this.includePendingTransactions = includePendingTransactions;
    }
}
