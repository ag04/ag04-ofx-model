package com.ag04.ofx.api.extras.user;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Either userId or userProfile should be set not both.
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("AG04.USERINFO")
@XmlRootElement(name="AG04.USERINFO")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserInfoMessageSet {

	/**
	 * Actual user ID if already known, or preferred user ID if user can choose, A-32
	 */
	@XStreamAlias("USERID")
	@XmlElement(name="USERID")
	private String userId;
	
	/**
	 * Full user profile in case of new user or user info update.
	 * 
	 */
	@XStreamAlias("AG04.USERPROFILE")
	@XmlElement(name="AG04.USERPROFILE")
	private UserProfile userProfile;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("userId", userId);
		sbuilder.append("userProfile", userProfile);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}
	
}
