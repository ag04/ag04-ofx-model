package com.ag04.ofx.api.extras.user;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.ag04.ofx.api.user.UserData;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * AG04 Add on to standard OFX messages.
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("AG04.USERPROFILE")
@XmlRootElement(name="AG04.USERPROFILE")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserProfile extends UserData {

	@XStreamAlias("DTUSRACT")
	@XmlElement(name="DTUSRACT")
	private LocalDate activationDate;
	
	@XStreamAlias("DTLSTCHG")
	@XmlElement(name="DTLSTCHG")
	private DateTime lastChangeDate;

	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append(super.toString());
		sbuilder.append("activationDate", activationDate);
		sbuilder.append("lastChangeDate", lastChangeDate);
		return sbuilder.toString();
	}
	
	//--- set / get methods ---------------------------------------------------
	
	public LocalDate getActivationDate() {
		return activationDate;
	}

	public void setActivationDate(LocalDate activationDate) {
		this.activationDate = activationDate;
	}

	public DateTime getLastChangeDate() {
		return lastChangeDate;
	}

	public void setLastChangeDate(DateTime lastChangeDate) {
		this.lastChangeDate = lastChangeDate;
	}
	
}
