package com.ag04.ofx.api.extras.user;

/**
 * Created by dmadunic on 19/07/2016.
 */
public enum Gender {
    MALE, FEMALE, UNDISCLOSED;
}
