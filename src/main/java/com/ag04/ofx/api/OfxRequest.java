package com.ag04.ofx.api;

import com.ag04.ofx.api.bank.BankMessageSetRequest;
import com.ag04.ofx.api.creditcard.CreditCardMessageSetRequest;
import com.ag04.ofx.api.signup.SignupMessageSetRequest;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ag04.ofx.api.signon.SignOnMessageSetRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Top level container for OFX messages.
 * 
 * @author dmadunic
 *
 */
@XStreamAlias("OFX")
@XmlRootElement(name="OFX")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfxRequest implements Serializable {

	@XStreamAlias("SIGNONMSGSRQV1")
	@XmlElement(name="SIGNONMSGSRQV1")
	private SignOnMessageSetRequest signOnMessageSetRequest;
	
	@XStreamAlias("SIGNUPMSGSRQV1")
	@XmlElement(name="SIGNUPMSGSRQV1")
	private SignupMessageSetRequest signupMessageSetRequest;

	@XStreamAlias("BANKMSGSRQV1")
	@XmlElement(name="BANKMSGSRQV1")
	private BankMessageSetRequest bankMessageSetRequest;

	@XStreamAlias("CREDITCARDMSGSRQV1")
	@XmlElement(name="CREDITCARDMSGSRQV1")
	private CreditCardMessageSetRequest ccMessageSetRequest;
	
	@Override
	public String toString() {
		final ToStringBuilder sbuilder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		sbuilder.append("signOnMessageSetRequest", signOnMessageSetRequest);
		sbuilder.append("signupMessageSetRequest", signupMessageSetRequest);
		sbuilder.append("bankMessageSetRequest", bankMessageSetRequest);
		sbuilder.append("ccMessageSetRequest", ccMessageSetRequest);
		return sbuilder.toString();
	}

	/**
	 * Return all present TransactionRequests (Wrappers) on this OfxRequest.
	 *
	 * NOTE: SignOnRequest is not in this list since it is not Transaction.
	 *
	 * @return
     */
	public List<TransactionWrapperRequest> getAllTrxRequests() {
		List<TransactionWrapperRequest> wrappers = new ArrayList<TransactionWrapperRequest>();
		wrappers.addAll(signupMessageSetRequest.getAllTrxRequests());
		wrappers.addAll(bankMessageSetRequest.getAllTrxRequests());
		wrappers.addAll(ccMessageSetRequest.getAllTrxRequests());
		return wrappers;
	}
	
	//--- set / get methods ---------------------------------------------------

	public SignOnMessageSetRequest getSignOnMessageSetRequest() {
		return signOnMessageSetRequest;
	}

	public void setSignOnMessageSetRequest(
			SignOnMessageSetRequest signOnMessageSetRequest) {
		this.signOnMessageSetRequest = signOnMessageSetRequest;
	}

	public SignupMessageSetRequest getSignupMessageSetRequest() {
		return signupMessageSetRequest;
	}

	public void setSignupMessageSetRequest(SignupMessageSetRequest signupMessageSetRequest) {
		this.signupMessageSetRequest = signupMessageSetRequest;
	}

	public BankMessageSetRequest getBankMessageSetRequest() {
		return bankMessageSetRequest;
	}

	public void setBankMessageSetRequest(BankMessageSetRequest bankMessageSetRequest) {
		this.bankMessageSetRequest = bankMessageSetRequest;
	}

	public CreditCardMessageSetRequest getCcMessageSetRequest() {
		return ccMessageSetRequest;
	}

	public void setCcMessageSetRequest(CreditCardMessageSetRequest ccMessageSetRequest) {
		this.ccMessageSetRequest = ccMessageSetRequest;
	}
}
