package com.ag04.ofx.utils.xstream;

import com.ag04.ofx.api.account.creditcard.CreditCardAccountAggregate;
import com.ag04.ofx.api.account.creditcard.CreditCardAccountFromAggregate;
import com.ag04.ofx.api.account.creditcard.CreditCardAccountInfo;
import com.ag04.ofx.api.account.creditcard.CreditCardAccountToAggregate;
import com.ag04.ofx.api.bank.*;
import com.ag04.ofx.api.OfxFinancialInstitution;
import com.ag04.ofx.api.OfxResponse;
import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperResponse;
import com.ag04.ofx.api.account.AccountInfo;
import com.ag04.ofx.api.creditcard.CreditCardMessageSetResponse;
import com.ag04.ofx.api.creditcard.CreditCardStatementResponse;
import com.ag04.ofx.api.creditcard.CreditCardStatementTrxResponse;
import com.ag04.ofx.api.signup.AccountInfoResponse;
import com.ag04.ofx.api.signup.AccountInfoTrxResponse;
import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.ag04.ofx.api.account.bank.BankAccountInfo;
import com.ag04.ofx.api.account.bank.BankAccountToAggregate;
import com.ag04.ofx.api.common.Currency;
import com.ag04.ofx.api.common.OriginalCurrency;
import com.ag04.ofx.api.extras.user.UserInfoMessageSet;
import com.ag04.ofx.api.extras.user.UserProfile;
import com.ag04.ofx.api.signon.OfxSignOnResponse;
import com.ag04.ofx.api.signon.SignOnMessageSetResponse;
import com.ag04.ofx.api.signup.SignupMessageSetResponse;
import com.ag04.ofx.api.statement.*;
import com.ag04.ofx.api.user.EnrollmentResponse;
import com.ag04.ofx.api.user.EnrollmentTrxResponse;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * @author dmadunic
 *
 */
public class OfxResponseMarshaller extends BaseOfxMarshaller {
	
	public OfxResponseMarshaller() {
		xstream = new XStream();
		xstream.ignoreUnknownElements();
		xstream.registerConverter(new DateTimeConverter());
		xstream.registerConverter(new LocalDateConverter());
		xstream.setMode(XStream.NO_REFERENCES);
		
		xstream.processAnnotations(OfxResponse.class);
		xstream.processAnnotations(TransactionWrapperResponse.class);
		
		//sign on message set and response
		xstream.processAnnotations(SignOnMessageSetResponse.class);
		xstream.processAnnotations(OfxSignOnResponse.class);

		//sign up message set
		xstream.processAnnotations(SignupMessageSetResponse.class);
		
		// account info 
		xstream.processAnnotations(AccountInfoTrxResponse.class); 
		xstream.processAnnotations(AccountInfoResponse.class);
		xstream.processAnnotations(AccountInfo.class);
		xstream.processAnnotations(BankAccountInfo.class);
		xstream.processAnnotations(CreditCardAccountInfo.class);

		// user ...
		xstream.processAnnotations(EnrollmentTrxResponse.class);
		xstream.processAnnotations(EnrollmentResponse.class);
		
		// common elements ...
		xstream.processAnnotations(Currency.class);
		xstream.processAnnotations(OriginalCurrency.class);
		xstream.processAnnotations(OfxFinancialInstitution.class);
		xstream.processAnnotations(OfxStatus.class);
		
		xstream.processAnnotations(BankAccountFromAggregate.class);
		xstream.processAnnotations(BankAccountToAggregate.class);
		xstream.processAnnotations(CreditCardAccountAggregate.class);
		xstream.processAnnotations(CreditCardAccountFromAggregate.class);
		xstream.processAnnotations(CreditCardAccountToAggregate.class);

		xstream.processAnnotations(AvailableBalanceAggregate.class);
		xstream.processAnnotations(LedgerBalanceAggregate.class);
		xstream.processAnnotations(BaseStatementResponse.class);
		xstream.processAnnotations(StatementTransaction.class);
		xstream.processAnnotations(RewardProgramPointsInfo.class);

		xstream.processAnnotations(TransactionsDataAggregate.class);
		
		// bank
		xstream.processAnnotations(BankMessageSetResponse.class);

		xstream.processAnnotations(StatementTrxResponse.class);
		xstream.processAnnotations(StatementResponse.class);

		xstream.processAnnotations(StatementClosingTrxResponse.class);
		xstream.processAnnotations(StatementClosingResponse.class);

		// credit card
		xstream.processAnnotations(CreditCardMessageSetResponse.class);

		xstream.processAnnotations(CreditCardStatementTrxResponse.class);
		xstream.processAnnotations(CreditCardStatementResponse.class);


		//--- ag04 extras -----------------------------------------------------

		xstream.processAnnotations(UserInfoMessageSet.class);
		xstream.processAnnotations(UserProfile.class);
		
	}
}
