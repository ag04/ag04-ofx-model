package com.ag04.ofx.utils.xstream;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.ag04.ofx.utils.OfxDateUtils;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
/**
 * 
 * @author dmadunic
 *
 */
public class LocalDateConverter extends AbstractSingleValueConverter {

	@Override
	public boolean canConvert(Class type) {
		return type.equals(LocalDate.class); 
	}

	@Override
	public Object fromString(String dateAsString) {
		DateTime dateTime = OfxDateUtils.parseDateTime(dateAsString);
		return dateTime.toLocalDate();
	}

	public String toString(Object obj) {
		LocalDate date = (LocalDate) obj;
		return OfxDateUtils.toOfxDateFormat(date);
	}

}
