package com.ag04.ofx.utils.xstream;

import com.ag04.ofx.api.account.creditcard.CreditCardAccountFromAggregate;
import com.ag04.ofx.api.bank.BankMessageSetRequest;
import com.ag04.ofx.api.OfxFinancialInstitution;
import com.ag04.ofx.api.OfxRequest;
import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.TransactionWrapperRequest;
import com.ag04.ofx.api.creditcard.CreditCardMessageSetRequest;
import com.ag04.ofx.api.creditcard.CreditCardStatementRequest;
import com.ag04.ofx.api.creditcard.CreditCardStatementTrxRequest;
import com.ag04.ofx.api.signup.AccountInfoRequest;
import com.ag04.ofx.api.signup.AccountInfoTrxRequest;
import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.ag04.ofx.api.common.Currency;
import com.ag04.ofx.api.signon.OfxSignOnRequest;
import com.ag04.ofx.api.signon.SignOnMessageSetRequest;
import com.ag04.ofx.api.signup.SignupMessageSetRequest;
import com.ag04.ofx.api.statement.IncludeTransactionsAggregate;
import com.ag04.ofx.api.bank.StatementClosingRequest;
import com.ag04.ofx.api.bank.StatementClosingTrxRequest;
import com.ag04.ofx.api.bank.StatementRequest;
import com.ag04.ofx.api.bank.StatementTrxRequest;
import com.ag04.ofx.api.user.EnrollmentRequest;
import com.ag04.ofx.api.user.EnrollmentTrxRequest;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * @author dmadunic
 *
 */
public class OfxRequestMarshaller extends BaseOfxMarshaller {
	
	public OfxRequestMarshaller() {
		xstream = new XStream();
		xstream.ignoreUnknownElements();
		xstream.registerConverter(new DateTimeConverter());
		xstream.setMode(XStream.NO_REFERENCES);
		
		xstream.processAnnotations(OfxRequest.class);
		xstream.processAnnotations(TransactionWrapperRequest.class);

		// common elements ...
		xstream.processAnnotations(Currency.class);
		xstream.processAnnotations(OfxFinancialInstitution.class);
		xstream.processAnnotations(OfxStatus.class);

		// sign on message set and requests
		xstream.processAnnotations(SignOnMessageSetRequest.class);
		xstream.processAnnotations(OfxSignOnRequest.class);

		// sign up message set
		xstream.processAnnotations(SignupMessageSetRequest.class);

		//-- account info ---
		xstream.processAnnotations(AccountInfoTrxRequest.class); 
		xstream.processAnnotations(AccountInfoRequest.class);
		xstream.processAnnotations(BankAccountFromAggregate.class);
		xstream.processAnnotations(CreditCardAccountFromAggregate.class);

		//-- user ---
		xstream.processAnnotations(EnrollmentTrxRequest.class);
		xstream.processAnnotations(EnrollmentRequest.class);

		// bank
		xstream.processAnnotations(BankMessageSetRequest.class);

		//creditcard
		xstream.processAnnotations(CreditCardMessageSetRequest.class);

		//-- statements ---
		xstream.processAnnotations(StatementRequest.class);
		xstream.processAnnotations(StatementTrxRequest.class);
		xstream.processAnnotations(StatementClosingTrxRequest.class);
		xstream.processAnnotations(StatementClosingRequest.class);

		xstream.processAnnotations(CreditCardStatementRequest.class);
		xstream.processAnnotations(CreditCardStatementTrxRequest.class);

		xstream.processAnnotations(IncludeTransactionsAggregate.class);
	}

}
