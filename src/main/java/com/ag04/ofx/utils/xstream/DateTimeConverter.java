package com.ag04.ofx.utils.xstream;

import org.joda.time.DateTime;

import com.ag04.ofx.utils.OfxDateUtils;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
/**
 * 
 * @author dmadunic
 *
 */
public class DateTimeConverter extends AbstractSingleValueConverter {

	@Override
	public boolean canConvert(Class type) {
		return type.equals(DateTime.class); 
	}

	@Override
	public Object fromString(String dateAsString) {
		return OfxDateUtils.parseDateTime(dateAsString);
	}

	public String toString(Object obj) {
		DateTime dateTime = (DateTime) obj;
		return OfxDateUtils.toOfxDateFormat(dateTime);
	}

}
