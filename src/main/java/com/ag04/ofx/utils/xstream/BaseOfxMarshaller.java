package com.ag04.ofx.utils.xstream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * 
 * @author dmadunic
 *
 */
public abstract class BaseOfxMarshaller {
	private static final Logger LOG = LoggerFactory.getLogger(BaseOfxMarshaller.class);
	
	protected XStream xstream;
	
	public Object marshall(String ofxString) {
		LOG.trace("STARTED ---> Transforming XML='{}'", ofxString);
		Object ofxEntity = xstream.fromXML(ofxString);
		LOG.debug("FINISHED --> Transforing String to Ofx Entity={}", ofxEntity);
		return ofxEntity;
	}

	public Object marshall(InputStream ofxStream) {
		LOG.trace("STARTED ---> Transforming XML='{}'", ofxStream);
		Object ofxEntity = xstream.fromXML(ofxStream);
		LOG.debug("FINISHED --> Transforing String to Ofx Entity={}", ofxEntity);
		return ofxEntity;
	}
	
	public String unmarshall(Object ofxEntity) {
		LOG.trace("STARTED ---> Transforming to XML from Ofx Entity={}", ofxEntity);
		String ofxString = xstream.toXML(ofxEntity);
		LOG.trace("FINISHED --> Transforing Ofx Entity to XML={}", ofxString);
		return ofxString;
	}

	public void unmarshall(Object ofxEntity, OutputStream entityStream) {
		LOG.trace("STARTED ---> Transforming to XML from Ofx Entity={}", ofxEntity);
		xstream.toXML ( ofxEntity, entityStream );
		LOG.trace("FINISHED --> Transforing Ofx Entity to XML!");
	}


}
