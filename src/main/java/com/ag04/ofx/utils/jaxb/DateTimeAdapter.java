package com.ag04.ofx.utils.jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;

import com.ag04.ofx.utils.OfxDateUtils;

/**
 * 
 * @author dmadunic
 *
 */
public class DateTimeAdapter extends XmlAdapter<String, DateTime> {
	
	@Override
	public DateTime unmarshal(String dateAsString) throws Exception {
		//return new DateTime(dateAsString);
		return OfxDateUtils.parseDateTime(dateAsString);
	}

	@Override
	public String marshal(DateTime dateTime) throws Exception {
		//return v.toString();
		return OfxDateUtils.toOfxDateFormat(dateTime);
	}

}
