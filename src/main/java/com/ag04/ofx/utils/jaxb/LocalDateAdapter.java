package com.ag04.ofx.utils.jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.ag04.ofx.utils.OfxDateUtils;
/**
 * 
 * @author dmadunic
 *
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

	@Override
	public LocalDate unmarshal(String dateAsString) throws Exception {
		DateTime dateTime = OfxDateUtils.parseDateTime(dateAsString);
		return dateTime.toLocalDate(); 
	}
	
	@Override
	public String marshal(LocalDate localDate) throws Exception {
		return OfxDateUtils.toOfxDateFormat(localDate);
	}


}
