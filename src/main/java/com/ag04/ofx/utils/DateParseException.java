package com.ag04.ofx.utils;
/**
 * Exception thrown by OfxDateParser when it was unable to parse supplied string date.
 * 
 * @author dmadunic
 *
 */
public class DateParseException extends RuntimeException {
	private static final long serialVersionUID = 1L;
		
	private String dateAsString;
	
	public DateParseException(String message, String dateAsString) {
		super(message);
		this.dateAsString = dateAsString;
	}

	public String getDateAsString() {
		return dateAsString;
	}

	public void setDateAsString(String dateAsString) {
		this.dateAsString = dateAsString;
	}
	
}
