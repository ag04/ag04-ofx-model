package com.ag04.ofx.utils;

import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ag04.ofx.api.OfxConstants;

/**
 * There is one format for representing dates, times, and time zones. The complete form is:
 * YYYYMMDDHHMMSS.XXX [gmt offset[:tz name]]
 * 
 * Date and datetime also accept values with fields omitted from the right. They assume the following defaults if a field is missing:
 * 
 * YYYYMMDD - 12:00 AM (the start of the day), GMT
 * YYYYMMDDHHMMSS - GMT 
 * YYYYMMDDHHMMSS.XXX - GMT

 * @author dmadunic
 *
 */
public class OfxDateUtils {
	private static final Logger LOG = LoggerFactory.getLogger(OfxDateUtils.class);
	
	private OfxDateUtils() {
		//
	}
	
	/**
	 * 
	 * @param dateAsOfxXmlString
	 * @return
	 * @throws DateParseException
	 */
	public static DateTime parseDateTime(String inputDate) throws DateParseException {
		LOG.trace("--->Parsing date='{}'", inputDate);
		if (StringUtils.isBlank(inputDate)) {
			LOG.debug("Empty date string received ---> nothing to do returning.", inputDate);
			return null;
		}
		TimeZone tz = determineTimeZone(inputDate);
		String dateAsOfxXmlString = removeTimeZoneFromInput(inputDate);
		LOG.trace("Sanatized inputDate='{}' and timeZone ={}", dateAsOfxXmlString, tz);
		int l = dateAsOfxXmlString.length();
		if ((l != OfxConstants.XML_DATE_SHORT_FORMAT.length()) && 
			(l != OfxConstants.XML_DATE_MEDIUM_FORMAT.length()) &&
			(l != OfxConstants.XML_DATE_LONG_FORMAT.length())
		) {
			LOG.error("Invalid format/lenght of date string='{}' ---> aborting", dateAsOfxXmlString);
			throw new DateParseException("Invalid format/lenght of date string='" + dateAsOfxXmlString + "'", dateAsOfxXmlString);
		}
		
		DateTime result = null;
		
//		result =  parseInternal(dateAsOfxXmlString, OfxConstants.XML_DATE_FULL_FORMAT, tz);
//		if (result != null) {
//			return result;
//		}
		result =  parseInternal(dateAsOfxXmlString, OfxConstants.XML_DATE_LONG_FORMAT, tz);
		if (result != null) {
			return result;
		}
		result =  parseInternal(dateAsOfxXmlString, OfxConstants.XML_DATE_MEDIUM_FORMAT, tz);
		if (result != null) {
			return result;
		}
		result =  parseInternal(dateAsOfxXmlString, OfxConstants.XML_DATE_SHORT_FORMAT, tz);
		if (result != null) {
			return result;
		}
		LOG.error("FAILED parsing date='{}' using any of the available patterns!", dateAsOfxXmlString);
		throw new DateParseException("Unable to parse date='" + dateAsOfxXmlString + "'", dateAsOfxXmlString);
	}
	
	public static String toOfxDateFormat(DateTime dateTime) {
		return toOfxDateFormat(dateTime, OfxConstants.XML_DATE_FULL_FORMAT);
	}
	
	public static String toOfxDateFormat(LocalDate date) {
		return toOfxDateFormat(date, OfxConstants.XML_DATE_SHORT_FORMAT);
	}
	
	public static String toOfxDateFormat(DateTime dateTime, String format) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
		String strOutputDateTime = formatter.print(dateTime);
		
		return strOutputDateTime;
	}
	
	public static String toOfxDateFormat(LocalDate date, String format) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
		String strOutputDateTime = formatter.print(date);
		
		return strOutputDateTime;
	}
	
	private static DateTime parseInternal(String inputDateString, String format, TimeZone tz) {
		try {
			DateTimeFormatter formatter = DateTimeFormat.forPattern(format).withZone(DateTimeZone.forTimeZone(tz));
			DateTime date = DateTime.parse(inputDateString, formatter);
			return date;
		} catch (IllegalArgumentException pe) {
			LOG.trace("Failed parsing '{}' using pattern={}, exception={}", inputDateString, format, pe);
			//pe.printStackTrace();
		}
		return null;
	}
	
	/**
	 * [
	 * @param date
	 * @return
	 */
	public static TimeZone determineTimeZone(String date) {
		int start = date.indexOf("[");
		if (date.indexOf("[") > 0) {
			int end = date.indexOf("]");
			int semicolonIndex = date.indexOf(":", start); 
			if (semicolonIndex > 0) {
				end = semicolonIndex;
			}
			String tzString = date.substring(start + 1, end);
			return TimeZone.getTimeZone("GMT" + tzString);
		}
		return TimeZone.getTimeZone("GMT");
	}
	
	public static String removeTimeZoneFromInput(String inputDate) {
		int start = inputDate.indexOf("[");
		if (inputDate.indexOf("[") > 0) {
			String tzString = inputDate.substring(0, start);
			return tzString;
		}
		return inputDate;
	}
	
}
