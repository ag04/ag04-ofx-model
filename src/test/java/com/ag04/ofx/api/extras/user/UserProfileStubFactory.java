package com.ag04.ofx.api.extras.user;

import org.joda.time.LocalDate;

/**
 * Created by domagoj on 20/08/16.
 */
public class UserProfileStubFactory {

    public static UserProfile kraljPera() {
        UserProfile userProfile = new UserProfile();
        userProfile.setActivationDate(LocalDate.now());
        userProfile.setAddressOne("Vlaška 70C");
        userProfile.setCity("Zagreb");
        userProfile.setDateOfBirth(LocalDate.now().minusYears(35));
        userProfile.setCountryCode("HRV");
        userProfile.setDayPhone("+385991234567");
        userProfile.setFirstName("Petar");
        userProfile.setLastName("Kralj");
        userProfile.setMail("petar.kralj@gmail.com");
        userProfile.setPostalCode("10000");
        userProfile.setTaxId("10000001");
        userProfile.setGender(Gender.MALE);

        return userProfile;
    }
}
