package com.ag04.ofx.api.account.bank;

import com.ag04.ofx.api.account.AccountStatus;

/**
 * Created by dmadunic on 20/08/2016.
 */
public class BankAccountInfoStubFactory {

    public static BankAccountInfo account(BankAccountType accttype, String accountNumber, String bankId, AccountStatus status) {
        BankAccountInfo accInfo = new BankAccountInfo();

        accInfo.setEnabledAsInterbankSource(Boolean.TRUE);
        accInfo.setEnabledAsInterbankDestination(Boolean.TRUE);
        accInfo.setTrxDownloadSupported(Boolean.TRUE);
        accInfo.setStatus(status);
        accInfo.setAccountAggregate(new BankAccountFromAggregate(new BankAccountAggregate(bankId, accountNumber, accttype)));

        return accInfo;
    }
}
