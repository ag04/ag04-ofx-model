package com.ag04.ofx.api.account;

import com.ag04.ofx.api.account.bank.BankAccountInfoStubFactory;
import com.ag04.ofx.api.account.bank.BankAccountType;

/**
 * Created by dmadunic on 20/08/2016.
 */
public class AccountInfoStubFactory {

    public static AccountInfo bankAccount(BankAccountType accttype, String accountNumber, String bankId, AccountStatus status) {
        AccountInfo info = new AccountInfo();
        info.setName("ACCOUNT NAME");
        info.setDescription("ACCOUNT DESCRIPTION");
        info.setPhone("+385991111111");
        info.setBankAccountInfo(BankAccountInfoStubFactory.account(accttype, accountNumber, bankId, status));
        return info;
    }
    
}
