package com.ag04.ofx.api.utils.jaxb;

import com.ag04.ofx.api.account.bank.BankAccountFromAggregate;
import com.ag04.ofx.api.account.bank.BankAccountType;
import com.ag04.ofx.api.bank.StatementResponse;
import com.ag04.ofx.api.common.BalanceAggregate;
import com.ag04.ofx.api.common.BalanceType;
import com.ag04.ofx.api.common.Currency;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmadunic on 01/08/2016.
 */
public class TestJaxbStatementResponse {

    public static void main(String args[]) throws Exception {
        StatementResponse response = new StatementResponse();

        response.setCurrency("HRK");

        BankAccountFromAggregate accountFrom = bankAccountFromAggregate();
        response.setBankAccountAggregate(accountFrom);


        List<BalanceAggregate> balances = new ArrayList<>();
        response.setBalances(balances);

        BalanceAggregate balEur = balanceAggregate("EUR", "100.00");
        BalanceAggregate balhrk = balanceAggregate("HRK", "700.00");
        response.getBalances().add(balEur);
        response.getBalances().add(balhrk);

        JAXBContext context = JAXBContext.newInstance(StatementResponse.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(response, new PrintWriter(System.out));
    }

    private static BalanceAggregate balanceAggregate(String currencyCode, String amount) {
        BalanceAggregate bal = new BalanceAggregate();
        bal.setType(BalanceType.DOLLAR);
        bal.setName("Some name");
        bal.setDescription("Some description");

        Currency currency = new Currency();
        currency.setCode(currencyCode);
        bal.setCurrency(currency);

        bal.setDate(DateTime.now());
        bal.setAmount(new BigDecimal(amount));
        return bal;
    }

    private static BankAccountFromAggregate bankAccountFromAggregate() {
        BankAccountFromAggregate acc = new BankAccountFromAggregate();
        acc.setAccountNumber("1234567890");
        acc.setBankId("PPLXLULL");
        acc.setType(BankAccountType.CHECKING);
        return acc;
    }
}
