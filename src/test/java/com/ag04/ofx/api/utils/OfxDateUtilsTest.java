package com.ag04.ofx.api.utils;
import static org.fest.assertions.Assertions.assertThat;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import com.ag04.ofx.utils.DateParseException;
import com.ag04.ofx.utils.OfxDateUtils;

/**
 * 
 * @author dmadunic
 *
 */
public class OfxDateUtilsTest {
	private static final String SHORT_FORMAT_DATE = "20140815";
	public static final String MEDIUM_FORMAT_DATE = SHORT_FORMAT_DATE + "135559";
	public static final String LONG_FORMAT_DATE = MEDIUM_FORMAT_DATE + ".001";
	public static final String FULL_FORMAT_DATE = LONG_FORMAT_DATE + "[+1]";
		
	@Test
	public void parseFullFormat_whenInputIsvalid() throws DateParseException {
		// arrange ...
		//act ...
		DateTime date = OfxDateUtils.parseDateTime(FULL_FORMAT_DATE);
		
		//assert ...
		assertThat(date).isNotNull();
		validateDate(date, FULL_FORMAT_DATE);
	}
	
	@Test
	public void parseLongFormat_whenInputIsvalid() throws DateParseException {
		// arrange ...
		
		//act ...
		DateTime date = OfxDateUtils.parseDateTime(LONG_FORMAT_DATE);
		
		//assert ...
		assertThat(date).isNotNull();
		validateDate(date, LONG_FORMAT_DATE);
	}
	
	@Test
	public void parseMediumFormat_whenInputIsvalid() throws DateParseException {
		// arrange ...
		
		//act ...
		DateTime date = OfxDateUtils.parseDateTime(MEDIUM_FORMAT_DATE);
		
		//assert ...
		assertThat(date).isNotNull();
		validateDate(date, MEDIUM_FORMAT_DATE);
	}
	
	@Test
	public void parseShortFormat_whenInputIsvalid() throws DateParseException {
		// arrange ...
		
		//act ...
		DateTime date = OfxDateUtils.parseDateTime(SHORT_FORMAT_DATE);
		
		//assert ...
		assertThat(date).isNotNull();
		validateDate(date, SHORT_FORMAT_DATE);
	}
	
	@Test(expected = DateParseException.class)
	public void parseInvalidString() throws DateParseException {
		// arrange ...
		
		//act ...
		DateTime date = OfxDateUtils.parseDateTime("201481");
		
		//assert ...
		assertThat(date).isNotNull();
		validateDate(date, SHORT_FORMAT_DATE);
	}
	
	@Test(expected = DateParseException.class)
	public void parseInvalidString2() throws DateParseException {
		// arrange ...
		
		//act ...
		OfxDateUtils.parseDateTime("2014-08-01");
	}
	
	//--- util methods --------------------------------------------------------
	
	
	private void validateDate(final DateTime date, String format) {
		DateTimeZone tz = date.getZone();
		if (FULL_FORMAT_DATE.equals(format)) {
		//	assertThat(tz).isNotNull();
		} else {
			
		}
		assertThat(date.getYear()).isEqualTo(2014);
		assertThat(date.getMonthOfYear()).isEqualTo(8);
		assertThat(date.getDayOfMonth()).isEqualTo(15);
		
		if (!SHORT_FORMAT_DATE.equals(format)) {
			
			assertThat(date.getHourOfDay()).isEqualTo(13);
			assertThat(date.getMinuteOfHour()).isEqualTo(55);
			assertThat(date.getSecondOfMinute()).isEqualTo(59);
			
			if (!MEDIUM_FORMAT_DATE.equals(format)) {
				assertThat(date.getMillisOfSecond()).isEqualTo(1);
			}
		} else {
			// start of the day - according to ofx specification
			assertThat(date.getHourOfDay()).isEqualTo(0);
			assertThat(date.getMinuteOfHour()).isEqualTo(0);
			assertThat(date.getSecondOfMinute()).isEqualTo(0);
		}

	}
}
