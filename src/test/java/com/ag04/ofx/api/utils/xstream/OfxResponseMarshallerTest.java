package com.ag04.ofx.api.utils.xstream;
import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;

import com.ag04.ofx.api.OfxSeverity;
import com.ag04.ofx.api.OfxStatus;
import com.ag04.ofx.api.account.AccountInfoStubFactory;
import com.ag04.ofx.api.account.AccountStatus;
import com.ag04.ofx.api.account.bank.BankAccountType;
import com.ag04.ofx.api.extras.user.Gender;
import com.ag04.ofx.api.extras.user.UserInfoMessageSet;
import com.ag04.ofx.api.extras.user.UserProfile;
import com.ag04.ofx.api.extras.user.UserProfileStubFactory;
import com.ag04.ofx.api.signup.SignupMessageSetResponse;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.junit.Test;

import com.ag04.ofx.api.OfxResponse;
import com.ag04.ofx.api.account.AccountInfo;
import com.ag04.ofx.api.signup.AccountInfoResponse;
import com.ag04.ofx.api.signup.AccountInfoTrxResponse;
import com.ag04.ofx.utils.xstream.OfxResponseMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author dmadunic
 *
 */

public class OfxResponseMarshallerTest {
	private static final Logger LOG = LoggerFactory.getLogger(OfxResponseMarshallerTest.class);
	
	OfxResponseMarshaller marshaller = new OfxResponseMarshaller();
	
	@Test
	public void testConvertOfxResponseToString_whenAllProperitesAreNull() {
		//arrange...
		OfxResponse response = new OfxResponse();
		
		//act ...
		String xml = marshaller.unmarshall(response);
		assertThat(xml).isNotNull();
	}
	
	@Test
	public void testConvertOfxResponseToString_whenAccountInfoTrxResponseIsSet_andAccountsIsEmptyList() {
		//arrange...
		OfxResponse response = new OfxResponse();
		
		AccountInfoTrxResponse accountInfoTrxResponse = new AccountInfoTrxResponse();
		accountInfoTrxResponse.setTransactionId("c5516b39-8f85-4060-af9f-1e61cd3f2548");
		accountInfoTrxResponse.setStatus(new OfxStatus(0L, OfxSeverity.INFO));
		
		AccountInfoResponse accountInfoResponse = new AccountInfoResponse();
		accountInfoResponse.setAccounts(new ArrayList<AccountInfo>());
		accountInfoResponse.setLastAccountUpdateDate(new DateTime());
		
		accountInfoTrxResponse.setResponse(accountInfoResponse);
		SignupMessageSetResponse signupMessageSetResponse = new SignupMessageSetResponse();

		signupMessageSetResponse.setAccountInfoTrxResponse(accountInfoTrxResponse);
		response.setSignupMessageSetResponse(signupMessageSetResponse);
		
		//act ...
		String xml = marshaller.unmarshall(response);
				
		assertThat(xml).isNotNull();
	}

	@Test
	public void testConvertOfxResponseToString_whenAccountInfoTrxResponseIsSet_andAccountsIsNotEmptyList() {
		//arrange...
		OfxResponse response = new OfxResponse();

		AccountInfoTrxResponse accountInfoTrxResponse = new AccountInfoTrxResponse();
		accountInfoTrxResponse.setTransactionId("c5516b39-8f85-4060-af9f-1e61cd3f2548");
		accountInfoTrxResponse.setStatus(new OfxStatus(0L, OfxSeverity.INFO));

		AccountInfoResponse accountInfoResponse = new AccountInfoResponse();
		AccountInfo accountInfo = AccountInfoStubFactory.bankAccount(BankAccountType.CHECKING, "9876543210", "02002", AccountStatus.ACTIVE);

		accountInfoResponse.addAccountInfo(accountInfo);
		accountInfoResponse.setLastAccountUpdateDate(new DateTime());

		accountInfoTrxResponse.setResponse(accountInfoResponse);
		SignupMessageSetResponse signupMessageSetResponse = new SignupMessageSetResponse();

		signupMessageSetResponse.setAccountInfoTrxResponse(accountInfoTrxResponse);
		response.setSignupMessageSetResponse(signupMessageSetResponse);

		//act ...
		String xml = marshaller.unmarshall(response);
		assertThat(xml).isNotNull();
	}

	@Test
	public void testConvertOfxResponseToString_whenuserProfileIsSet() {
		//arrange...
		UserInfoMessageSet userInfoMessageSet = new UserInfoMessageSet();

		UserProfile userProfile = UserProfileStubFactory.kraljPera();

		userInfoMessageSet.setUserProfile(userProfile);

		//act ...
		String xml = marshaller.unmarshall(userInfoMessageSet);

		assertThat(xml).isNotNull();
	}
}
