# ag04-ofx-model

AG04 Java representation of the OFX model, implemented with xstream anotations.

## Usage
### Requirements
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Gradle](https://gradle.org/gradle-download/)

### Setup (First time)
1. Clone the repository: `git clone https://YoutUserName@bitbucket.org/ag04/ag04-ofx-model.git`
4. Build project with: ` ./gradlew clean build `

## Changelog

### Version 0.0.19

* Dodao u Marshaller i Unnarshaller xstream.setMode(XStream.NO_REFERENCES);
* proširio constructor za: IncludeTransactionsAggregate 

#### Dependencies:

Same as in the previous version.

### Version 0.0.18

Minor bug fixes and formatting changes.

#### Dependencies:

Upgraded xstream to version: 1.4.9

### Version 0.0.17

Implement support for CreditCard Account elements:

* CCACCTINFO
    * CCACCTFROM
    * CCACCTTO
    
* CREDITCARDMSGSRQV1
    * CCSTMTTRNRQ
    * CCSTMTRQ

* CREDITCARDMSGSRSV1
    * CCSTMTTRNRS
    * CCSTMTRS

#### Dependencies:

Same as in the previous version.

### Version 0.0.16

* Introduced Java enums for AccountStatus and BankAccountType
* Removed AG04.INITIAL balance from BankAccountInfo

#### Dependencies:

Same as in the previous version.

### Version 0.0.15

Fixed AXB mapping for BAL element in StatementResponse.

#### Dependencies:

Same as in the previous version.

### Version 0.0.14

Added new enum Gender and added it as ag04 exstension, AG04.GENDER property, to the UserData object.

#### Dependencies:

Same as in the previous version.


### Version 0.0.13

Upgraded spring-boot to version 1.3.6.RELEASE and cleaned up unnecessary lib/jar version definitions so that now all dependencies are resolved through spring.
Also upgraded Gradle to 2.14.1 version.

#### Dependencies:

* spring-boot :: 1.3.6.RELEASE
    * spring-boot-starter
    * spring-boot-starter-test

* joda-time :: 2.8.2 (resolved through spring-boot-parent)

* org.apache.commons :: commons-lang3 :: 3.4

* xstream :: 1.4.7

* junit :: 4.12 (resolved through spring-boot-parent)
* mockito-core :: 1.10.19 (resolved through spring-boot-parent)

* javassist :: 3.20.0-GA
* fest-assert :: 1.4
